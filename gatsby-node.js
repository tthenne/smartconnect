const path = require(`path`)
const fs = require(`fs`)

const slugify = (string) => {
  const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
  const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
  const p = new RegExp(a.split('').join('|'), 'g')

  return string.toString().toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w\-]+/g, '') // Remove all non-word characters
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '') // Trim - from end of text
}

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        "~": path.resolve(__dirname, "src")
      }
    }
  })
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    type WpPage_Producttour_sections implements Node {
      slug: String 
    }

    type WpPage_Pricing_questionAnswer implements Node {
      slug: String
    }

    type WpPage_Overview_industries implements Node {
      slug: String
    }

    type WpPage_Overview_who_useCase implements Node {
      slug: String
    }

    type WpPage_Cloudvsonpremisetable_table implements Node {
      slug: String
    }
  `
  createTypes(typeDefs)
}

exports.createResolvers = ({
  createResolvers
}) => {
  createResolvers({
    WpPage_Producttour_sections: {
      slug: {
        resolve: source => slugify(source.title)
      }
    },
    WpPage_Pricing_questionAnswer: {
      slug: {
        resolve: source => slugify(source.question)
      }
    },
    WpPage_Overview_industries: {
      slug: {
        resolve: source => slugify(source.name)
      }
    },
    WpPage_Overview_who_useCase: {
      slug: {
        resolve: source => slugify(source.feature)
      }
    },
    WpPage_Cloudvsonpremisetable_table: {
      slug: {
        resolve: source => slugify(source.feature)
      }
    },
  })
}

exports.createPages = async ({ graphql, actions: { createPage } }) => {
  try {
    const { data, errors } = await graphql(`
      query loadPages {
        allWpPage {
          edges {
            node {
              slug
              template {
                templateName
              }
            } 
          }
        }
        allWpCaseStudy {
          edges {
            node {
              slug
            }
          }
        }
        allWpKnowledgeBase {
          edges {
            node {
              slug
            }
          }
        }
        allWpTemplate {
          edges {
            node {
              slug
            }
          }
        }
      }
    `)
    if (errors) {
      throw errors
    }
    data.allWpPage.edges.forEach(({ node: page }) => {
      if(page.slug === "home") return
      
      const template = page.slug
      const component = fs.existsSync(path.resolve(`./src/templates/pages/${template}.js`)) ? template : `default`
      createPage({
        path: `/${page.slug}`,
        component: path.resolve(`./src/templates/pages/${component}.js`),
        context: {
          slug: page.slug
        }
      })
    })
    //create case studies 
    data.allWpCaseStudy.edges.forEach(({ node: study }) => {
      createPage({
        path: `/case-study/${study.slug}`,
        component: path.resolve(`./src/templates/case-study.js`),
        context: {
          slug: study.slug
        }
      })
    })
    //create knowledge base articles
    data.allWpKnowledgeBase.edges.forEach(({ node: kb }) => {
      createPage({
        path: `/knowledge-base/${kb.slug}`,
        component: path.resolve(`./src/templates/kb.js`),
        context: {
          slug: kb.slug
        }
      })
    })
    //create template pages
    data.allWpTemplate.edges.forEach(({ node: template }) => {
      createPage({
        path: `/template/${template.slug}`,
        component: path.resolve(`./src/templates/template.js`),
        context: {
          slug: template.slug
        }
      })
    })
  } catch (e) {
    console.log("Error:")
    console.error(e)
  }
}