import React, { createContext, useContext, useReducer } from "react"

const MenuContext = createContext()

const DEFAULT_STATE = {
  active: null 
}

const reducer = (state, action) => {
  switch(action.type){
    case 'activate':
      return { ...state, active: action.payload }
    case 'deactivate':
      return { ...state, active: null }
    default:
      return DEFAULT_STATE
  }
}

const MenuProvider = ({children}) => (
  <MenuContext.Provider value={useReducer(reducer, DEFAULT_STATE)}>
    {children}
  </MenuContext.Provider>
)

const useMenu = () => useContext(MenuContext) 

export {MenuProvider, useMenu}