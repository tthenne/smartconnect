import {useEffect, useState} from "react"

export const useWindowScroll = () => {
  const threshold = 100
  const initial = typeof window !== `undefined` ? true : window.pageYOffset > threshold
  const [inverse, setInverse] = useState(initial);

  useEffect(() => {
    // Handler to call on window resize
    function handleScroll() {
      setInverse(window.pageYOffset > threshold);
    }
    
    // Add event listener
    window.addEventListener("scroll", handleScroll);
    
    // Call handler right away so state gets updated with initial window size
    handleScroll();
    
    // Remove event listener on cleanup
    return () => window.removeEventListener("scroll", handleScroll);
  }, []); // Empty array ensures that effect is only run on mount

  return inverse;
}