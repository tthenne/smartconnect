import React, { createContext, useContext, useState } from "react"

const MobileContext = createContext()

const MobileProvider = ({ children }) => (
  <MobileContext.Provider value={useState(false)}>
    { children }
  </MobileContext.Provider>
)

const useMobile = () => useContext(MobileContext)

export {MobileProvider, useMobile}