import React, { createContext, useContext, useReducer } from "react"

const TabContext = createContext()

const DEFAULT_STATE = {
  active: 0 
}

const reducer = (state, action) => {
  switch(action.type){
    case 'activate':
      return {...state, active: action.payload} 
    case 'deactive':
      return {...state, active: null}
    default:
      return DEFAULT_STATE
  } 
}

const TabProvider = ({ children }) => (
  <TabContext.Provider value={useReducer(reducer, DEFAULT_STATE)}>
    { children }
  </TabContext.Provider>
)

const useTabs = () => useContext(TabContext)

export { TabProvider, useTabs }