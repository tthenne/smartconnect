/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled, useThemeUI } from "theme-ui"
import { useEffect } from "react"
import { graphql, Link } from "gatsby"
import { connect } from "react-redux"


import Layout from "~/components/layout"
import SEO from "~/components/seo"
import { CHANGE_THEME } from "~/state/actions/header"
import SolutionCard from "~/components/marketplace/solutionCard"

const Template = ({ dispatch, data: { template } }) => {
  useEffect(() => {
    dispatch({ type: CHANGE_THEME, payload: 'default' })
  }, [dispatch])
  const { theme } = useThemeUI()
  return (
    <Layout>
      <SEO title={`${template.title} | Template`} />
      <section
        sx={{
          pt: '12em',
          pb: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                pl: 4,
                pr: 5,
                width: ['full', '1/4'],
              }}>
              <Flex
                sx={{
                  flexDirection: 'column'
                }}>
                <Box
                  sx={{
                    py: 2,
                    mb: 2
                  }}
                  as="aside">
                  {
                    template.integrationSolutionDetails.wpcfIcon && (
                      <img
                        sx={{
                          width: 24
                        }}
                        src={template.integrationSolutionDetails.wpcfIcon.localFile.publicURL} alt={template.title} />
                    )
                  }
                </Box>
                <Box
                  sx={{
                    py: 2,
                    mb: 4
                  }}
                  as="aside">
                  <Styled.h4>
                    {template.title}
                  </Styled.h4>
                </Box>
                <Box
                  sx={{
                    py: 2,
                    mb: 4
                  }}
                  as="aside">
                  <Styled.h4>
                    Supports
                    </Styled.h4>
                  <span>
                    {template.versionSupportDetails.wpcfVersionSupport}
                  </span>
                </Box>
                <Box
                  sx={{
                    py: 2,
                    mb: 4
                  }}
                  as="aside">
                  <Styled.h4>
                    Publisher
                    </Styled.h4>
                  <span>
                    {template.templateDetails.wpcfPublisher}
                  </span>
                </Box>
                <Box
                  sx={{
                    py: 2,
                    mb: 2
                  }}
                  as="aside">
                  <Link
                    sx={{
                      ...theme.buttons.blue
                    }}
                    to={template.templateDetails.wpcfDownloadFile}>
                    Download
                        </Link>
                </Box>
                <Box
                  sx={{
                    py: 2,
                    mb: 4
                  }}
                  as="aside">
                  <Link
                    sx={{
                      ...theme.buttons.blue
                    }}
                    to={`/contact`}>
                    Implement for Me
                        </Link>
                </Box>
              </Flex>
            </Box>
            <Box
              sx={{
                pl: 5,
                pr: 4,
                width: ['full', '3/4']
              }}>
              <Flex
                sx={{
                  flexDirection: 'column'
                }}>
                <Box
                  sx={{
                    mb: 4
                  }}>
                  <Styled.h4
                    sx={{
                      mb: 3
                    }}>Overview</Styled.h4>
                  <div
                    sx={{
                      ul: {
                        listStyle: 'none',
                        p: 0
                      }
                    }}
                    dangerouslySetInnerHTML={{
                      __html: template.overviewTab.wpcfOverviewText
                    }} />
                </Box>
              </Flex>
              {template.integrationSolutionDetails.related && (
                <section
                  sx={{
                    py: 5,
                    mt: 4,
                    borderTop: theme => `1px solid ${theme.colors.gray[3]}`,
                  }}>
                  <Container
                    sx={{
                      px: 4
                    }}>
                    <Flex
                      sx={{
                        mx: -4,
                        flexWrap: 'wrap',
                        mb: 4
                      }}>
                      <Box
                        sx={{
                          px: 4,
                          width: ['full']
                        }}>
                        <Styled.h3>Recommended Integration Solutions</Styled.h3>
                      </Box>
                    </Flex>
                    <Flex
                      sx={{
                        mx: -4,
                        flexWrap: 'wrap'
                      }}>
                      {template.integrationSolutionDetails.related.map(solution => (
                        <Box
                          key={solution.databaseId}
                          sx={{
                            width: ['full', '1/3'],
                            px: 2,
                            mb: 4
                          }}>
                          <SolutionCard
                            solution={solution} />
                        </Box>
                      ))}
                    </Flex>
                  </Container>
                </section>
              )}
            </Box>
          </Flex>
        </Container>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query TemplatePage($slug: String!) {
    template: wpTemplate(slug: { eq: $slug }){
      slug
      title
      integrationSolutionDetails {
        wpcfIcon {
          localFile {
            publicURL
          }
        }
        related {
          __typename
          ...on WpTemplate {
            __typename
            databaseId
            title
            slug
            integrationSolutionDetails {
              wpcfIcon {
                localFile {
                  publicURL
                }
              }
              wpcfShortDescription
              wpcfHighlight
            }
            smartConnectProducts {
              nodes {
                termTaxonomyId
              }
            }
          }
          ...on WpConnection {
            __typename
            databaseId
            title
            slug
            integrationSolutionDetails {
              wpcfIcon {
                localFile {
                  publicURL
                }
              }
              wpcfShortDescription
              wpcfHighlight
            }
            smartConnectProducts {
              nodes {
                termTaxonomyId
              }
            }
          }
          ...on WpService {
            __typename
            databaseId
            title
            slug
            integrationSolutionDetails {
              wpcfIcon {
                localFile {
                  publicURL
                }
              }
              wpcfShortDescription
              wpcfHighlight
            }
            smartConnectProducts {
              nodes {
                termTaxonomyId
              }
            }
          }
        }
      }
      templateDetails {
        wpcfPublisher
        wpcfDownloadFile
      }
      versionSupportDetails {
        wpcfVersionSupport
        wpcfOnlineVersionSupport
      }
      overviewTab {
        wpcfOverviewText
      }
    }
  }
`

export default connect()(Template)