/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled, useThemeUI } from "theme-ui"
import { graphql } from "gatsby"

import Layout from "~/components/layout"
import SEO from "~/components/seo"
import TopFooter from "~/components/footer/top"

import Resource from "~/components/caseStudy/resource"
import { MdPlayCircleOutline } from "react-icons/md"
import { FaQuoteLeft } from "react-icons/fa"

/* eslint-disable jsx-a11y/control-has-associated-label */
const CaseStudy = ({ data: { study } }) => {
  const { theme } = useThemeUI()
  return (
    <Layout>
      <SEO title={`${study.title}`} />
      <section
        sx={{
          pt: 6,
          pb: 5
        }}></section>
      <section
        sx={{
          pt: 6,
          backgroundImage: `url('${study.featuredImage.node.localFile.publicURL}')`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: '50%'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'flex-end'
            }}>
            <Box
              sx={{
                width: ['full', '2/3'],
                py: 5,
                px: 6,
                bg: 'white'
              }}>
              <Flex
                sx={{
                  flexDirection: 'column'
                }}>
                <Box>
                  <strong>Customer Case Study</strong>
                </Box>
                <Box
                  sx={{
                    my: 4
                  }}>
                  <Styled.h1
                    sx={{
                      fontSize: 7
                    }}>
                    {study.title}
                  </Styled.h1>
                </Box>
                <Box>
                  <img src={study.caseStudyFields.logo.localFile.publicURL} alt={study.title} />
                </Box>
                <Box>
                  <a
                    sx={{
                      ...theme.buttons.gray
                    }}
                    href={study.caseStudyFields.callToAction}>
                    Learn More
                  </a>
                </Box>
              </Flex>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full'],
                textAlign: 'center'
              }}>
              <p sx={{ fontSize: 3, fontWeight: 'bold' }}>{study.caseStudyFields.summary}</p>
            </Box>
          </Flex>
        </Container>
      </section>
      <section>
        <Container
          sx={{
            px: 5
          }}>
          <Flex
            sx={{
              mx: -5,
              flexWrap: 'wrap'
            }}>
            <Flex
              sx={{
                p: 5,
                bg: 'gray.3',
                width: ['full'],
                flexWrap: 'wrap'
              }}>
              <Box
                sx={{
                  px: 4,
                  width: ['full', '1/2', '1/4']
                }}>
                <Styled.h5>Industy</Styled.h5>
                <p>{study.caseStudyFields.highlights.industry}</p>
              </Box>
              <Box
                sx={{
                  px: 4,
                  width: ['full', '1/2', '1/4']
                }}>
                <Styled.h5>Systems Used</Styled.h5>
                <p>{study.caseStudyFields.highlights.systemsUsed}</p>
              </Box>
              <Box
                sx={{
                  px: 4,
                  width: ['full', '1/2', '1/4']
                }}>
                <Styled.h5>Software Used</Styled.h5>
                <p>{study.caseStudyFields.highlights.softwareUsed}</p>
              </Box>
              <Box
                sx={{
                  px: 4,
                  width: ['full', '1/2', '1/4']
                }}>
                <Styled.h5>PDF Download</Styled.h5>
                <p>
                  <a href={study.caseStudyFields.highlights.pdfDownload.localFile.publicURL}><MdPlayCircleOutline size="2em" /></a></p>
              </Box>
            </Flex>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '3/4']
              }}>
              <Styled.h1
                sx={{
                  mb: 4
                }}>Case Study</Styled.h1>
              <div dangerouslySetInnerHTML={{
                __html: study.content
              }} />
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5,
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '3/4']
              }}>
              <div
                sx={{
                  position: 'relative',
                  p: 5,
                  bg: 'gray.1'
                }}>
                <FaQuoteLeft
                  sx={{
                    position: 'absolute',
                    top: 4,
                    left: 5,
                    color: 'gray.6'
                  }}
                  size="2em" />
                <div
                  sx={{
                    fontWeight: 'bold'
                  }}
                  dangerouslySetInnerHTML={{
                    __html: study.caseStudyFields.testimonial.testimonialACF.quote
                  }} />
                <p>- {study.caseStudyFields.testimonial.testimonialACF.name}</p>
              </div>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          pt: 5,
          pb: 6,
          my: 6,
          position: 'relative',
        }}>
          <div
            sx={{
              position: 'absolute',
              right: '10vw',
              top: 0,
              bg: 'gray.1',
              width: ['full', '1/2', '5/12'],
              height: 'full',
            }}></div>
        <Container
          sx={{
            px: 4,
            position: 'relative',
            zIndex: 1
          }}>
          
          <Flex
            sx={{
              position: 'relative',
              mx: -4,
              mb: 5,
              flexWrap: 'wrap',
              zIndex: 1
            }}>
            <Box
              sx={{
                px: 4,
              }}>
              <Styled.h1>Resources</Styled.h1>
            </Box>
          </Flex>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            {study.caseStudyFields.resources.map((resource, i) => (
              <Box
                key={i}
                sx={{
                  px: 4,
                  mb: 4,
                  width: ['full', '1/2', '1/3']
                }}>
                <Resource
                  resource={resource} />
              </Box>
            ))}
          </Flex>
        </Container>
      </section>
      <TopFooter /> 
    </Layout>
  )
}

export const query = graphql`
  query CaseStudyQuery($slug: String!){
    study: wpCaseStudy( slug: { eq: $slug }) {
      title
      slug
      featuredImage {
        node {
          localFile {
            publicURL
          }
        }
      }
      content
      caseStudyFields {
        logo {
          localFile {
            publicURL
          }
        }
        callToAction
        summary
        highlights {
          industry
          systemsUsed
          softwareUsed
          pdfDownload {
            localFile {
              publicURL
            }
          }
        }
        testimonial{
          ...on WpTestimonial {
            databaseId
            testimonialACF {
              name
              title
              quote
            }
          }
        }
        resources{
          title
          description
          link
        }
      }
    }
  }
`

export default CaseStudy 