/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled, Button } from "theme-ui"
import { useEffect, useState } from "react"
import { Link, graphql } from "gatsby"
import { connect } from "react-redux"
import Img from "gatsby-image"

import { CHANGE_THEME } from "~/state/actions/header"
import Layout from "~/components/layout"
import SEO from "~/components/seo"
import Modal from "~/components/modal"
import Industry from "~/components/overview/industry"
import BusinessProfessionals from "~/components/overview/businessProfessionals"
import Developers from "~/components/overview/developers"
import Consultants from "~/components/overview/consultants"
import FooterTop from "~/components/footer/top"

const OverviewPage = ({ data, dispatch }) => {
  useEffect(() => {
    dispatch({ type: CHANGE_THEME, payload: "light" })
  }, [dispatch])
  const [activeIndustry, setIndustry] = useState(data.wpPage.overview.industries[0].slug)
  const [open, setModal] = useState(false)

  return (
    <Layout>
      <SEO title="Product Tour" />
      <section
        sx={{
          pt: '12em',
          pb: 5,
          bg: '#7797D5'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '1/2']
              }}>
              <Styled.h1
                sx={{
                  color: 'white',
                  fontSize: 7,
                }}>SmartConnect<br />Overview</Styled.h1>
              <p
                sx={{
                  fontSize: 2,
                  color: 'white'
                }}>Looking to deliver integration<br />
                  outcomes faster and at lower cost?</p>
            </Box>
            <Flex
              sx={{
                flexWrap: 'wrap',
                width: ['full', '1/2'],
                px: 4,
                mx: -2,
                mb: -6,
              }}>
              <Box
                sx={{
                  px: 2,
                  mb: 3,
                  width: ['1/2', '1/3']
                }}>
                <Img fluid={data.allFile.nodes.find(f => f.name === "overview-1").childImageSharp.fluid} alt={`SmartConnect Product Overview`} />
              </Box>
              <Box
                sx={{
                  px: 2,
                  mb: 3,
                  width: ['1/2', '1/3']
                }}>
                <Img fluid={data.allFile.nodes.find(f => f.name === "overview-2").childImageSharp.fluid} alt={`SmartConnect Product Overview`} />
              </Box>
              <Box
                sx={{
                  px: 2,
                  mb: 3,
                  width: ['1/2', '1/3']
                }}>
                <Img fluid={data.allFile.nodes.find(f => f.name === "overview-3").childImageSharp.fluid} alt={`SmartConnect Product Overview`} />
              </Box>
              <Box
                sx={{
                  px: 2,
                  width: ['1/2', '1/3']
                }}>
                <Img fluid={data.allFile.nodes.find(f => f.name === "overview-4").childImageSharp.fluid} alt={`SmartConnect Product Overview`} />
              </Box>
              <Box
                sx={{
                  px: 2,
                  width: ['1/2', '1/3']
                }}>
                <Img fluid={data.allFile.nodes.find(f => f.name === "overview-5").childImageSharp.fluid} alt={`SmartConnect Product Overview`} />
              </Box>
              <Box
                sx={{
                  px: 2,
                  width: ['1/2', '1/3']
                }}>
                <Img fluid={data.allFile.nodes.find(f => f.name === "overview-6").childImageSharp.fluid} alt={`SmartConnect Product Overview`} />
              </Box>
            </Flex>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          pt: 6,
          pb: 4
        }}>
        <Container>
          <Flex
            sx={{
              flexWrap: 'wrap',
              justifyContent: 'center',
              mx: 'auto',
              width: ['full', '3/4']
            }}>
            <Box
              sx={{
                px: 3,
                width: ['1/6'],
                textAlign: ['left', 'right']
              }}>
              <a
                sx={{
                  color: 'text'
                }}
                onClick={e => {
                  e.preventDefault()
                  setModal(true)
                }}
                href="/#">
                <img src={data.allFile.nodes.find(({ name }) => name === "play").publicURL} alt="Play Video" />
              </a>
            </Box>
            <Flex
              sx={{
                flexDirection: 'column',
                px: 3,
                width: ['full', '5/6']
              }}>
              <a
                sx={{
                  color: 'text'
                }}
                onClick={e => {
                  e.preventDefault()
                  setModal(true)
                }}
                href="/#">
                <strong><u>watch the video</u></strong>
              </a>
              <p>While business requirements get complicated, the way you deliver shouldn’t be.  Standardize on SmartConnect and deliver faster business outcomes.</p>
            </Flex>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '2/3'],
                textAlign: 'center'
              }}>
              <Styled.h1
                sx={{
                  fontSize: 7
                }}>How SmartConnect can help you automate in all your teams.</Styled.h1>
            </Box>
          </Flex>
          <Flex
            sx={{
              mt: 5,
              flexDirection: 'column'
            }}>
            <Flex
              sx={{
                justifyContent: 'center',
                mb: 5
              }}>
              {data.wpPage.overview.industries.map(industry => (
                <Box
                  key={industry.slug}
                  sx={{
                    mx: 3
                  }}>
                  <Button
                    onClick={e => {
                      e.preventDefault()
                      setIndustry(industry.slug)
                    }}
                    sx={{
                      bg: 'transparent',
                      color: 'primary',
                      border: theme => `1px solid ${theme.colors.primary}`,
                      px: 4,
                      py: 2,
                    }}>{industry.name}</Button>
                </Box>
              ))}
            </Flex>
            <Industry
              industry={data.wpPage.overview.industries.find(i => i.slug === activeIndustry)} />
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Flex
              sx={{
                p: 5,
                bg: '#F2F0EE',
                width: ['full', '11/12'],
                flexDirection: 'column'
              }}>
              <Box>
                <Styled.h6>Make your investments go further.</Styled.h6>
              </Box>
              <Flex
                sx={{
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  flexWrap: 'wrap'
                }}>
                <Box>
                  <Styled.h2>Ready to deliver your next project on-time and on budget?</Styled.h2>
                </Box>
                <Box
                  sx={{
                    flexShrink: 1,
                    mt: [4, 0]
                  }}>
                  <Link
                    to={`/pricing`}
                    sx={{
                      bg: 'transparent',
                      color: 'primary',
                      border: theme => `1px solid ${theme.colors.primary}`,
                      borderRadius: 'full',
                      px: 4,
                      py: 2,
                    }}>See Pricing</Link>
                </Box>
              </Flex>
            </Flex>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 4
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Box
              sx={{
                px: 4,
              }}>
              <Styled.h1
                sx={{
                  fontSize: 7
                }}>Who is SmartConnect for?</Styled.h1>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5,
          backgroundImage: `url("${data.allFile.nodes.find(({ name }) => name === 'business-professionals-bg').publicURL}")`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '8vw center'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center'
            }}>
            <BusinessProfessionals
              who={data.wpPage.overview.who[0]} /> 
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5,
          backgroundColor: 'gray.1',
          backgroundImage: `url("${data.allFile.nodes.find(({ name }) => name === 'developers-bg').publicURL}")`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '60vw bottom'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center'
            }}>
            <Developers
              who={data.wpPage.overview.who[1]} /> 
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          pt: 5,
          pb: 6,
          backgroundImage: `url("${data.allFile.nodes.find(({ name }) => name === 'it-consultant-bg').publicURL}")`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'contain',
          backgroundPosition: '8vw top'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center'
            }}>
            <Consultants
              who={data.wpPage.overview.who[2]} /> 
          </Flex>
        </Container>
      </section>
      <FooterTop />
      { open && (
        <Modal
          close={e => {
            setModal(false)
          }}>
          <iframe title="overview" width="560" height="315" src="https://www.youtube.com/embed/IFF3CsXkmkc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </Modal>
      )}
    </Layout>
  )
}

export const query = graphql`
  query OverviewPageQuery($slug: String!){
    allFile(filter: {relativeDirectory: {eq: "overview"}}){
      nodes {
        id
        name
        publicURL
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          } 
        }
      }
    }
    wpPage( slug: { eq: $slug }) {
      title
      databaseId
      overview {
        industries {
          slug
          name
          subtitle
          features {
            title
            description
          }
        }
        who {
          jobTitle
          description
          useCase {
            slug
            feature
            content
            thumbnail {
              localFile {
                publicURL
              }
            }
          }
        }
      }
    }
  }
`

export default connect()(OverviewPage)