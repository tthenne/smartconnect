/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled } from "theme-ui"
import { useEffect } from "react"
import { graphql, Link } from "gatsby"
import { connect } from "react-redux"
import Img from "gatsby-image"

import { CHANGE_THEME } from "~/state/actions/header"
import Layout from "~/components/layout"
import SEO from "~/components/seo"
import Table from "~/components/cloudVsOnPremise/table.js"

const CloudOnPremisePage = ({ data, dispatch }) => {
  useEffect(() => {
    dispatch({ type: CHANGE_THEME, payload: "default" })
  }, [dispatch])

  return (
    <Layout>
      <SEO title="SmartConnect vs. SmartConnect.com" />
      <section
        sx={{
          pt: '12em',
          pb: 5,
          bg: '#88B07E'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '7/12']
              }}>
              <Styled.h1
                sx={{
                  fontSize: 8,
                  lineHeight: 'none',
                  mb: 3
                }}>{data.wpPage.title}</Styled.h1>
              <span sx={{ color: 'primary' }}>We offer both.</span>
            </Box>
            <Box
              sx={{
                px: 4,
                width: ['full', '5/12'],
                mb: -6
              }}>
              <Img fluid={data.wpPage.featuredImage.node.localFile.childImageSharp.fluid} alt={data.wpPage.title} />
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          pt: 5,
          pb: 4
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Flex
              sx={{
                flexDirection: 'column',
                px: 4,
                width: ['full', '2/3']
              }}>
              <strong sx={{ fontSize: 3 }}><u>the choice is yours with smartconnect</u></strong>
              <p>Run your integration where you want to. Deliver your integrations in the cloud with SmartConnect.com or on-premise with SmartConnect.</p>
            </Flex>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '3/4']
              }}>
              <Styled.h1
                sx={{
                  fontSize: [6, 7]
                }}>What’s the difference between<br />
                  SmartConnect and SmartConnect.com?</Styled.h1>
              <p>Both products (SmartConnect and SmartConnect.com) are fully-featured integration solutions that allow you to handle integrations with your cloud and on-premise applications.The main difference is where the integration process runs.</p>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center',
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '5/12']
              }}>
              <Styled.h1>SmartConnect<br />(on-premise)</Styled.h1>
              <p>SmartConnect is eOne’s on-premise integration solution, so it is installed into and runs within a Windows environment.</p>
            </Box>
            <Box
              sx={{
                backgroundImage: `url('${data.allFile.nodes.find(({ name }) => name === "smartconnect-on-premise").publicURL}')`,
                backgroundPosition: 'right center',
                backgroundRepeat: 'no-repeat',
                px: 4,
                width: ['full', '7/12']
              }}>
              <Box
                sx={{
                  width: 'sm',
                  bg: 'white',
                  py: 4,
                  px: 5,
                  mt: '20em',
                  boxShadow: 'xl',
                  borderRadius: 20
                }}><p>SmartConnect on-premise brings the suite of SmartConnect products to you.</p></Box>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5,
          bg: 'gray.1'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center',
            }}>
            <Flex
              sx={{
                backgroundImage: `url('${data.allFile.nodes.find(({ name }) => name === "smartconnect-cloud").publicURL}')`,
                backgroundPosition: 'left center',
                backgroundRepeat: 'no-repeat',
                px: 4,
                width: ['full', '1/2'],
                justifyContent: 'flex-end'
              }}>
              <Box
                sx={{
                  width: 'sm',
                  bg: 'white',
                  py: 4,
                  px: 5,
                  mt: '20em',
                  boxShadow: 'xl',
                  borderRadius: 20
                }}><p>SmartConnect.com allows the suite of SmartConnect products to be accessed anywhere.</p></Box>
            </Flex>
            <Box
              sx={{
                px: 4,
                width: ['full', '1/2']
              }}>
              <Styled.h1>SmartConnect<br />(cloud)</Styled.h1>
              <p>SmartConnect.com runs in Microsoft Azure, so getting started simply requires you to create an account, log in and start integrating.  No environment, no servers and no dedicated IT required.</p>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 6
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Flex
              sx={{
                p: 5,
                bg: '#F2F0EE',
                width: ['full', '11/12'],
                flexDirection: 'column'
              }}>
              <Box>
                <Styled.h6>Make your investments go further.</Styled.h6>
              </Box>
              <Flex
                sx={{
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  flexWrap: 'wrap'
                }}>
                <Box>
                  <Styled.h2>Ready to deliver your next project on-time and on budget?</Styled.h2>
                </Box>
                <Box
                  sx={{
                    flexShrink: 1,
                    mt: [4, 0]
                  }}>
                  <Link
                    to={`/pricing`}
                    sx={{
                      bg: 'transparent',
                      color: 'primary',
                      border: theme => `1px solid ${theme.colors.primary}`,
                      borderRadius: 'full',
                      px: 4,
                      py: 2,
                    }}>See Pricing</Link>
                </Box>
              </Flex>
            </Flex>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full']
              }}>
              <Styled.h1
                sx={{
                  fontSize: 7
                }}>Cloud vs On-Premise</Styled.h1>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          pb: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Table
             table={data.wpPage.cloudVsOnPremiseTable.table} /> 
          </Flex>
        </Container>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query CloudOnPremiseQuery($slug: String!){
    allFile(filter: {relativeDirectory: {eq: "cloud"}}){
      nodes {
        id
        name
        publicURL
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          } 
        }
      }
    } 
    wpPage( slug: { eq: $slug }) {
      title
      databaseId
      featuredImage {
        node {
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
      cloudVsOnPremiseTable {
        table {
          slug
          feature
          header
          smartconnect
          onpremise
        }
      }
    }
  }
`

export default connect()(CloudOnPremisePage)