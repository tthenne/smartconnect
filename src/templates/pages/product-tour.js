/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled } from "theme-ui"
import { useEffect } from "react"
import { graphql } from "gatsby"
import { connect } from "react-redux"

import { CHANGE_THEME } from "~/state/actions/header"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import Section from "../../components/productTour/section"
import Table from "~/components/cloudVsOnPremise/table.js"

const ProductTour = ({ dispatch, data: { page } }) => {
  useEffect(() => {
    dispatch({ type: CHANGE_THEME, payload: "default" })
  }, [dispatch])
  return (
    <Layout>
      <SEO title="Product Tour" />
      <section
        sx={{
          pt: '12em',
          pb: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4
            }}>
            <Flex
              sx={{
                flexDirection: 'column',
                width: ['full'],
                px: 4
              }}>
              <Box
                sx={{
                  width: 'full'
                }}>
                <div
                  sx={{
                    minHeight: '450px',
                    width: 'fulll',
                    bg: 'gray.2'
                  }} />
              </Box>
            </Flex>
          </Flex>
        </Container>
      </section>

      {page.productTour.sections.map((section, i) => {
        const even = i % 2
        return (
          <Section
            key={i}
            bg={even ? 'gray.1' : 'white'}
            even={even}
            section={section} />
        )
      })}

      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full']
              }}>
              <Styled.h1
                sx={{
                  fontSize: 7
                }}>Cloud vs On-Premise</Styled.h1>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          pb: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Table
             table={page.cloudVsOnPremiseTable.table} /> 
          </Flex>
        </Container>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query ProductTourQuery($slug: String!){
    page: wpPage( slug: { eq: $slug }) {
      title
      productTour {
        sections {
          title
          slug
          subtitle
          contents {
            title
            content
          }
          icon {
            localFile {
              publicURL
            }
          }
          screenshot {
            localFile {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
      cloudVsOnPremiseTable {
        table {
          slug
          feature
          header
          smartconnect
          onpremise
        }
      }
    }
  }
`

export default connect()(ProductTour)