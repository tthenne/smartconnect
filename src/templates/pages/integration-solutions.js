/** @jsx jsx */
import { jsx, Container, Flex, Box, Styled, useThemeUI } from "theme-ui"
import { useEffect } from "react"
import { connect } from "react-redux"
import { Link, graphql } from "gatsby"

import * as connectionActions from "../../state/actions/connections"
import * as templateActions from "../../state/actions/templates"
import * as serviceActions from "../../state/actions/services"
import { CHANGE_THEME } from "~/state/actions/header"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

import FilterForm from "../../components/marketplace/filterForm"
import Search from "../../components/marketplace/filterForm/search"
import Connections from "../../components/marketplace/connections"
import Templates from "../../components/marketplace/templates"
import Services from "../../components/marketplace/services"

const IntegrationSolutions = ({ location, solutions, dispatch, data }) => {
  const { theme } = useThemeUI()
  useEffect(() => {
    //dispatch header theme
    dispatch({ type: CHANGE_THEME, payload: 'default' })
    //hydrate the redux store with all connections
    dispatch({ type: connectionActions.ALL_CONNECTIONS, payload: data.allWpConnection.nodes })
    //hydrate the redux store with all templates 
    dispatch({ type: templateActions.ALL_TEMPLATES, payload: data.allWpTemplate.nodes })
    //hydrate the redux store with all services 
    dispatch({ type: serviceActions.ALL_SERVICES, payload: data.allWpService.nodes })

  }, [data, dispatch, location])
  return (
    <Layout>
      <SEO title="Integration Solutions" />
      { /** Marketing Hero */}
      <section
        sx={{
          pt: '12em',
          pb: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -2,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 2,
                width: ['full', '3/4'],
                mb: [4, 0]
              }}>
              <Flex
                sx={{
                  px: 4,
                  py: 5,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  width: 'full',
                  height: 'full',
                  background: `url('${data.page.integrationSolutions.connectionHighlight.image.localFile.publicURL}') no-repeat center / cover`,
                  borderRadius: 10
                }}>
                <Styled.h1
                  sx={{
                    color: 'primary',
                    fontWeight: 'black',
                    textTransform: 'uppercase'
                  }}>{data.page.integrationSolutions.connectionHighlight.title}</Styled.h1>
                <div
                  dangerouslySetInnerHTML={{
                    __html: data.page.integrationSolutions.connectionHighlight.description
                  }} />
                <Box
                  sx={{
                    mt: 4
                  }}>
                  <Link
                    to={data.page.integrationSolutions.connectionHighlight.link}
                    sx={{
                      ...theme.buttons.blue,
                      fontSize: 1
                    }}>Learn More</Link>
                </Box>
              </Flex>
            </Box>
            <Box
              sx={{
                px: 2,
                width: ['full', '1/4']
              }}>
              <Flex
                sx={{
                  px: 4,
                  py: 4,
                  flexDirection: 'column',
                  width: 'full',
                  height: 'full',
                  background: `url('${data.page.integrationSolutions.marketing.image.localFile.publicURL}') no-repeat center / cover`,
                  borderRadius: 10
                }}>
                <Styled.h3
                  sx={{
                    mt: 0,
                    color: 'white',
                    fontWeight: 'black',
                  }}>{data.page.integrationSolutions.marketing.title}</Styled.h3>
                <p
                  sx={{
                    color: 'white'
                  }}>{data.page.integrationSolutions.marketing.tagline}</p>
                <Box
                  sx={{
                    mt: 'auto'
                  }}>
                  <Link
                    to={data.page.integrationSolutions.marketing.link}
                    sx={{
                      px: 3,
                      py: 2,
                      bg: `rgba(255,255,255,0.9)`,
                      borderRadius: 'full',
                      color: 'gray.7',
                      fontSize: 1
                    }}>Learn More</Link>
                </Box>
              </Flex>
            </Box>
          </Flex>
        </Container>
      </section>
      <section>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full']
              }}>
              <Styled.h1
                sx={{
                  color: 'primary',
                  fontWeight: 'black'
                }}>Rapid Integration For All Your Apps</Styled.h1>
              <p
                sx={{
                  fontSize: 0
                }}>Save time &amp; accelerate your project with eOne’s app connections, templates and service offerings.</p>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: [4, 5]
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '1/4'],
                borderRight: theme => `1px solid ${theme.colors.gray[3]}`,
                order: [2, 0]
              }}>
              <FilterForm
                products={data.allWpScProduct.nodes}
                connections={data.allWpScConnection.nodes}
                categories={data.allWpScCategories.nodes} />
            </Box>
            <Flex
              sx={{
                px: 4,
                flexDirection: 'column',
                width: ['full', '3/4'],
                order: 1
              }}>
              <Search />
              {(solutions.includes("connections") || solutions.length === 0) && (
                <Connections />
              )}
              {(solutions.includes("templates") || solutions.length === 0) && (
                <Templates />
              )}
              {(solutions.includes("services") || solutions.length === 0) && (
                <Services />
              )}
            </Flex>
          </Flex>
        </Container>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query SolutionsQuery($slug: String!) {
    page: wpPage(slug: { eq: $slug }){
      title
      integrationSolutions {
        connectionHighlight {
          title
          description
          link
          image {
            localFile {
              publicURL
            }
          }
        }
        marketing {
          title
          tagline
          link
          image {
            localFile {
              publicURL
            }
          }
        }
      }
    }
    allWpConnection(sort: {order: ASC, fields: date}){
      nodes {
        __typename
        databaseId
        title
        slug
        integrationSolutionDetails{
          wpcfShortDescription
          wpcfHighlight
          wpcfIcon {
            localFile {
              publicURL
            }
          }
        }
        smartConnectConnections {
          nodes {
            name
            termTaxonomyId
          }
        }
        smartConnectProducts {
          nodes {
            name
            termTaxonomyId
          }
        }
        smartConnectCategories {
          nodes {
            name
            termTaxonomyId
          }
        }
      }
    }
    allWpTemplate(sort: {order: ASC, fields: date}){
      nodes {
        __typename
        databaseId
        title
        slug
        integrationSolutionDetails{
          wpcfShortDescription
          wpcfHighlight
          wpcfIcon {
            localFile {
              publicURL
            }
          }
        }
        smartConnectConnections {
          nodes {
            name
            termTaxonomyId
          }
        }
        smartConnectProducts {
          nodes {
            name
            termTaxonomyId
          }
        }
        smartConnectCategories {
          nodes {
            name
            termTaxonomyId
          }
        }
      }
    }
    allWpService(sort: {order: ASC, fields: date}){
      nodes {
        __typename
        databaseId
        title
        slug
        integrationSolutionDetails{
          wpcfShortDescription
          wpcfHighlight
          wpcfIcon {
            localFile {
              publicURL
            }
          }
        }
        smartConnectConnections {
          nodes {
            name
            termTaxonomyId
          }
        }
        smartConnectProducts {
          nodes {
            name
            termTaxonomyId
          }
        }
        smartConnectCategories {
          nodes {
            name
            termTaxonomyId
          }
        }
      }
    }
    allWpScProduct {
      nodes {
        name
        termTaxonomyId
      }
    }
    allWpScConnection {
      nodes {
        name
        termTaxonomyId
      }
    }
    allWpScCategories {
      nodes {
        name
        termTaxonomyId
      }
    }
  }
`

const mapStateToProps = state => ({
  solutions: state.selected.solutions
})

export default connect(mapStateToProps)(IntegrationSolutions)