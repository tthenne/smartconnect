/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled } from "theme-ui"
import { useEffect } from "react"
import { connect } from "react-redux"
import { graphql } from "gatsby"

import { CHANGE_THEME } from "~/state/actions/header"
import { ALL_KNOWLEDGE_BASE } from "~/state/actions/knowledge"
import Layout from "~/components/layout"
import SEO from "~/components/seo"
import KbHeader from "~/components/kb/header"
import Category from "~/components/kb/category"

const KnowledgeBasePage = ({ dispatch, data }) => {
  useEffect(() => {
    dispatch({ type: CHANGE_THEME, payload: "default" })
    dispatch({ type: ALL_KNOWLEDGE_BASE, payload: data.allWpKnowledgeBase.nodes })
  }, [dispatch, data])

  return (
    <Layout>
      <SEO title="Knowledge Base" />
      <KbHeader
        title={data.wpPage.title}
        subtitle={`Your questions, answered.`} />
      <section
        sx={{
          py: 6
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              mb: 4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full']
              }}>
              <Styled.h3
                sx={{
                  fontSize: 5
                }}>Browse Common Categories</Styled.h3>
            </Box>
          </Flex>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            {data.allWpKbCategory.nodes.map(category => (
              <Box
                key={category.termTaxonomyId}
                sx={{
                  px: 4,
                  width: ['full', '1/2', '1/3']
                }}>
                <Category
                  category={category} />
              </Box>
            ))}
          </Flex>
        </Container>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query KnowledgeBasePage ($slug: String!) {
    wpPage( slug: { eq: $slug }){
      databaseId
      title
    }
    allWpKbCategory {
      nodes {
        termTaxonomyId
        name
        knowledgeBaseArticles {
          nodes {
            date
            title
            slug
          }
        }
      }
    }
    allWpKnowledgeBase {
      nodes {
        databaseId
        slug
        title
      }
    }
  }
`

export default connect()(KnowledgeBasePage)