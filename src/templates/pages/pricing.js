/** @jsx jsx */
import { jsx, Flex, Box, Button, Container, Styled } from "theme-ui"
import { useState } from "react"
import { connect } from "react-redux"
import { CHANGE_THEME } from "~/state/actions/header"
import { Link, graphql } from "gatsby"
import { BiPlusCircle, BiMinusCircle } from "react-icons/bi"

import { slugify } from "~/helpers"

import Layout from "~/components/layout"
import SEO from "~/components/seo"
import Switch from "~/components/switch"
import Plan from "~/components/pricing/plan"
import TableHeader from "~/components/pricing/tableHeader"
import PricingRow from "~/components/pricing/pricingRow"
import MobileRow from "~/components/pricing/mobileRow"
import QuestionAnswer from "~/components/pricing/questionAnswer"
import { useEffect } from "react"

const connectionExamples = [
  {
    id: 0,
    title: '1 Dynamics GP Instance = 1 Connection',
    sub: true 
  },
  {
    id: 1,
    title: '1 Salesforce Instance = 1 Connection',
  },
  {
    id: 2,
    title: '1 Dynamics Business Central Instance = 1 Connection',
    sub: true
  },
  {
    id: 3,
    title: '1 SQL Instance =  1 Connection',
  },
  {
    id: 4,
    title: '1 Dynamics NAV Instance = 1 Connection',
    sub: true
  },
  {
    id: 5,
    title: '1 REST Web Service Connection Configuration = 1 Connection',
  },
  {
    id: 6,
    title: '1 Dynamics 365 Instance = 1 Connection',
    sub: true
  },
]
const ConnectionCount = ({ title, sub = false }) => {
  return (
    <Flex
      sx={{
        height: 'full',
        flexDirection: 'column',
        justifyContent: 'center',
        p: 3,
        borderBottom: theme => `1px solid ${theme.colors.gray[2]}`
      }}>
      <strong><u>{ title }</u></strong>
      { sub && (
        <small sx={{ fontSize: 0 }}>(Multi-Company Integration Included)</small>
      )}
    </Flex>
  )
}

const Pricing = ({ data: { allFile, page }, dispatch }) => {
  useEffect(() => {
    dispatch({ type: CHANGE_THEME, payload: "default" })
  }, [dispatch])
  const types = ["subscription", "perpetual"]
  const [activeType, setActiveType] = useState(types[0])
  const [compare, toggleCompare] = useState(false)
  const [activeQA, setQA] = useState('')

  return (
    <Layout>
      <SEO title="Product Tour" />
      {/** Pricing Page Header/Intro */}
      <section
        sx={{
          pt: '12em',
          pb: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full'],
                textAlign: 'center'
              }}>
              <Styled.h1
                sx={{
                  color: 'primary',
                  fontSize: 7
                }}>{page.title}</Styled.h1>
              <div
                dangerouslySetInnerHTML={{
                  __html: page.content
                }} />
            </Box>
          </Flex>
        </Container>
      </section>
      {/** Pricing Bucket */}
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
            }}>
            <Box
              sx={{
                width: ['full', '3/4'],
                px: 4,
                mx: 'auto'
              }}>
              <Flex
                sx={{
                  flexWrap: 'wrap',
                  alignItems: 'center'
                }}>
                <Box
                  sx={{
                    px: 4,
                    width: ['full', '1/2']
                  }}>
                  <Styled.h2
                    sx={{
                      fontSize: 6,
                      lineHeight: 'tight',
                      color: 'primary'
                    }}>Get the right plan for your team.</Styled.h2>
                </Box>
                <Box
                  sx={{
                    px: 4,
                    width: ['full', '1/2'],
                    textAlign: 'right'
                  }}>
                  <Switch
                    change={t => {
                      setActiveType(t)
                    }}
                    active={activeType}
                    options={types} />
                </Box>
              </Flex>
            </Box>
          </Flex>
          <Flex
            sx={{
              mt: 5,
              mx: -3,
              flexWrap: 'wrap'
            }}>
            {page.pricing.plans.map(plan => {
              const p = {
                ...plan,
                highlight: plan.title === "Business" ? true : false
              }
              return (
                <Box
                  key={plan.title}
                  sx={{
                    px: 3,
                    width: ['full', '1/3'],
                    mb: [5, 0]
                  }}>
                  <Plan
                    type={activeType}
                    plan={p} />
                </Box>
              )
            })}
          </Flex>
        </Container>
      </section>
      {/** Pricing Compare/Table */}
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '1/2'],
                mb: [4, 0]
              }}>
              <Styled.h1
                sx={{
                  fontSize: 7,
                  color: 'primary'
                }}>Compare all features</Styled.h1>
            </Box>
            <Box
              sx={{
                px: 4,
                width: ['full', '1/2'],
                textAlign: ['left', 'center']
              }}>
              <Button
                onClick={e => {
                  toggleCompare(!compare)
                }}
                sx={{
                  display: 'flex',
                  mx: [0, 'auto'],
                  alignItems: 'center',
                  fontSize: 1,
                }}
                variant='blue'>
                <span sx={{ mr: 2 }}>View all Comparisons</span>
                {compare ? <BiMinusCircle /> : <BiPlusCircle />}
              </Button>
            </Box>
          </Flex>
        </Container>
      </section>
      {compare && (
        <section
          sx={{
            pt: 4,
            pb: 5
          }}>
          <Container
            sx={{
              px: 4
            }}>
            <Flex
              sx={{
                flexWrap: 'wrap',
                alignItems: 'center'
              }}>
              <Box
                sx={{
                  width: ['full', '1/3']
                }}>
                <Switch
                  change={t => {
                    setActiveType(t)
                  }}
                  active={activeType}
                  options={types} />
              </Box>
              <Flex
                sx={{
                  width: ['full', '2/3'],
                  flexWrap: 'wrap'
                }}>
                {page.pricing.plans.map((plan, i, plans) => {
                  const key = plan.title.toLowerCase()
                  const p = {
                    ...plan,
                    highlight: plan.title === "Business" ? true : false
                  }
                  return (
                    <Box
                      key={key}
                      sx={{
                        width: ['full', '1/3'],
                        mb: [5, 0]
                      }}>
                      <TableHeader
                        type={activeType}
                        plan={p}
                        index={i} />
                      { page.pricing.table[activeType].map((row, r, rows) => (
                        <MobileRow
                          key={slugify(row.feature)}
                          row={row}
                          plan={key}
                          last={r === rows.length - 1} />
                      ))}
                    </Box>
                  )
                })}
              </Flex>
            </Flex>
            <Flex
              sx={{
                display: ['none', 'flex'],
                flexDirection: 'column'
              }}>
              {page.pricing.table[activeType].map((row, i, plans) => (
                <PricingRow
                  key={slugify(row.feature)}
                  row={row}
                  index={i}
                  last={i === plans.length - 1} />
              ))}
            </Flex>
          </Container>
        </section>
      )}
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full']
              }}>
              <Styled.h1
                sx={{
                  fontSize: 7,
                  mb: 1
                }}>
                Definitions
              </Styled.h1>
            </Box>
          </Flex>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
              }}>
              <Link
                to={`/pricing#number`}
                sx={{
                  fontWeight: 'bold',
                  textDecoration: 'underline',
                  color: 'primary'
                }}>What does number of connections mean?</Link>
            </Box>
            <Box
              sx={{
                px: 4,
              }}>
              <Link
                to={`/pricing#counts`}
                sx={{
                  fontWeight: 'bold',
                  textDecoration: 'underline',
                  color: 'primary'
                }}>What counts as a connection?</Link>
            </Box>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4,
            backgroundImage: `url('${allFile.nodes.find(({ name }) => name === "connection-count").publicURL}')`,
            backgroundSize: '100%',
            backgroundClip: 'content-box',
            pb: 7
          }}>
          <Flex
            sx={{
              p: 5,
              pb: 0,
              flexWrap: 'wrap',
              justifyContent: 'flex-end',
              mb: -7
            }}>
            <Flex
              sx={{
                py: 4,
                px: 5,
                flexDirection: 'column',
                width: ['full', '2/3'],
                bg: 'white'
              }}>
              <Box>
                <Styled.h1
                  id="number"
                  sx={{
                    fontSize: 5,
                    lineHeight: 'tight'
                  }}>
                  What does the #<br />of connections mean?
                  </Styled.h1>
              </Box>
              <Box>
                <p>In each plan, you’ll receive a certain # of connections.  This means the # of connections that can be used in each install of SmartConnect or within a SmartConnect.com environment.</p>
              </Box>
              <Box>
                <p><strong>example</strong> In the case of SmartConnect Basic Subscription Plan, SmartConnect On-Premise (4 installs) & SmartConnect.com are included.  The customer has rights to:</p>
              </Box>
              <Box
                sx={{
                  my: 3
                }}>
                <strong><u>smartconnect.com environment</u></strong>
                <br />
                <strong>2 connections</strong>
              </Box>
              <Box
                sx={{
                  my: 3
                }}>
                <strong><u>smartconnect.com on-premise</u></strong>
                <br />
                <strong>4 installs with 2 connections per deployment</strong>
              </Box>
              <Flex
                sx={{
                  flexDirection: 'column',
                  fontSize: 1
                }}>
                <Flex
                  sx={{
                    flexWrap: 'wrap',
                    mx: -3,
                    alignItems: 'center',
                  }}>
                  <Box
                    sx={{
                      px: 3,
                      width: ['full', '1/2']
                    }}><p>SmartConnect On-Premise Production Install Up to 2 Connections</p>
                  </Box>
                  <Box
                    sx={{
                      px: 3,
                      width: ['full', '1/2']
                    }}><p>SmartConnect On-Premise Test Install Up to 2 Connections</p>
                  </Box>
                </Flex>
                <Flex
                  sx={{
                    flexWrap: 'wrap',
                    mx: -3,
                    alignItems: 'center',
                  }}>
                  <Box
                    sx={{
                      px: 3,
                      width: ['full', '1/2']
                    }}><p>SmartConnect On-Premise Dev Install
                        Up to 2 Connections</p>
                  </Box>
                  <Box
                    sx={{
                      px: 3,
                      width: ['full', '1/2']
                    }}><p>SmartConnect On-Premise UAT Install
                        Up to 2 Connections</p>
                  </Box>
                </Flex>
              </Flex>
            </Flex>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 6,
          bg: 'gray.1'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              mb: 5,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full']
              }}>
              <Styled.h2
                id="counts"
                sx={{
                  fontSize: 6,
                  mb: 1
                }}>
                What counts as a connection?
              </Styled.h2>
            </Box>
          </Flex>
          <Flex
            sx={{
              mb: 4,
              flexDirection: 'column'
            }}>
            <Flex
              sx={{
                mx: -4,
                mb: 4,
                flexWrap: 'wrap'
              }}>
              <Box
                sx={{
                  px: 4,
                  width: ['full', '1/2']
                }}>
                <Styled.h5>
                  Unlimited Integration with your Configured Connections
                  </Styled.h5>
                <p>Connections are configured once and can then be used as a source or destination in many (unlimited) integrations.</p>
              </Box>
              <Box
                sx={{
                  px: 4,
                  width: ['full', '1/2']
                }}>
                <Styled.h5>
                  Unlimited Integration with your Configured Connections
                  </Styled.h5>
                <p>Connections are configured once and can then be used as a source or destination in many (unlimited) integrations.</p>
              </Box>
            </Flex>
            <Flex
              sx={{
                mx: -4,
                flexWrap: 'wrap'
              }}>
              <Box
                sx={{
                  px: 4,
                  width: ['full', '1/2']
                }}>
                <Styled.h5>
                  Unlimited Integration with your Configured Connections
                  </Styled.h5>
                <p>Connections are configured once and can then be used as a source or destination in many (unlimited) integrations.</p>
              </Box>
            </Flex>
          </Flex>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
              { connectionExamples.map(example => (
                <Box
                  key={example.id}
                  sx={{
                    px: 4,
                    mb: 3,
                    width: ['full', '1/2']
                  }}>
                  <ConnectionCount {...example} /> 
                </Box>
              ))}
            
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              mb: 5,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                textAlign: 'center'
              }}>
               <Styled.h1
                 sx={{
                   fontSize: 7,
                   mb: 3
                 }}>Questions?</Styled.h1> 
               <Styled.h5>We have answers</Styled.h5>
            </Box>
          </Flex>
          <Flex
            sx={{
              mx: -4,
              flexDirection: 'column'
            }}>
            { page.pricing.questionAnswer.map(qa => (
              <QuestionAnswer
                key={qa.slug}
                question={qa.question}
                answer={qa.answer}
                slug={qa.slug}
                activate={ setQA }
                active={ qa.slug === activeQA } />
            ))} 
          </Flex>
        </Container>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query PricingQuery($slug: String!){
    allFile(filter: {relativeDirectory: {eq: "pricing"}}){
      nodes {
        id
        name
        publicURL
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          } 
        }
      }
    }
    page: wpPage( slug: { eq: $slug }){
      databaseId
      title
      content
      pricing {
        plans {
          title
          subscription {
            price
            features
            link
          }
          perpetual {
            price
            features
            link
          }
        }
        table {
          subscription {
            separator
            feature
            basic {
              included
              text
            }
            business {
              included
              text
            }
            premium {
              included
              text
            }
          }
          perpetual {
            separator
            feature
            basic {
              included
              text
            }
            business {
              included
              text
            }
            premium {
              included
              text
            }
          }
        }
        questionAnswer {
          slug
          question
          answer
        }
      } 
    }
  }
`
export default connect()(Pricing)