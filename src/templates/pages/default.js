import React, { Fragment } from "react"
import { graphql } from "gatsby"

const Page = ({ data }) => {
  return (
    <Fragment>
      <pre>{ JSON.stringify(data, null, 2) }</pre>
    </Fragment>
  )
}

export const query = graphql`
  query PageQuery($slug: String!){
    wpPage( slug: { eq: $slug }) {
      title
      databaseId
    }
  }
`

export default Page