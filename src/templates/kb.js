/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled } from "theme-ui"
import { useEffect } from "react"
import { connect } from "react-redux"
import { graphql } from "gatsby"

import { CHANGE_THEME } from "~/state/actions/header"
import { ALL_KNOWLEDGE_BASE } from "~/state/actions/knowledge"
import Layout from "~/components/layout"
import SEO from "~/components/seo"
import KbHeader from "~/components/kb/header"
import Category from "~/components/kb/category"
import Support from "~/components/kb/support"

const KnowledgeBase = ({ dispatch, data: { kb, allWpKbCategory, allWpKnowledgeBase } }) => {
  useEffect(() => {
    dispatch({ type: CHANGE_THEME, payload: "default" })
    //fill the store with all knowledgebase for search feature
    dispatch({ type: ALL_KNOWLEDGE_BASE, payload: allWpKnowledgeBase.nodes })
  }, [dispatch, allWpKnowledgeBase])
  return (
    <Layout>
      <SEO title={kb.title} />
      <KbHeader
        title={`Knowledge Base`}
        subtitle={kb.title} />
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              my: 4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full']
              }}>
              <Styled.h3
                sx={{
                  fontSize: 4,
                }}>Browse Common Categories</Styled.h3>
            </Box>
          </Flex>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Flex
              sx={{
                flexDirection: 'column',
                px: 4,
                width: ['full', '1/3']
              }}>
              {allWpKbCategory.nodes.map(category => (
                <Category
                  active={kb.slug}
                  key={category.termTaxonomyId}
                  category={category} />
              ))}
            </Flex>
            <Flex
              sx={{
                flexDirection: 'column',
                px: 4,
                width: ['full', '2/3']
              }}>
              <Box>
                <Styled.h3
                  sx={{
                    mt: 0
                  }}>{kb.title}</Styled.h3>
              </Box>
              <Box>
                <div dangerouslySetInnerHTML={{
                  __html: kb.content
                }} />
              </Box>
              <Support />
            </Flex>
          </Flex>
        </Container>
      </section>
    </Layout>
  )
}

export const query = graphql`
  query KnowledgeBaseArticle($slug: String!) {
    kb: wpKnowledgeBase( slug: { eq: $slug }) {
      slug
      title
      content
    }
    allWpKnowledgeBase {
      nodes {
        databaseId
        slug
        title
      }
    }
    allWpKbCategory {
      nodes {
        termTaxonomyId
        name
        knowledgeBaseArticles {
          nodes {
            date
            title
            slug
          }
        }
      }
    }
  }
`

export default connect()(KnowledgeBase)