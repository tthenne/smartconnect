/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled } from "theme-ui"
import { connect } from "react-redux"
import { graphql, useStaticQuery } from "gatsby"

import SearchForm from "~/components/kb/searchForm"
import KbList from "~/components/kb/list"

const KbHeader = ({ title = '', subtitle = '', filteredKnowledgeBase }) => {
  const data = useStaticQuery(graphql`
    query {
      allFile( filter: { name: { in: ["kb-bg"] }}){
        nodes {
          id
          name
          publicURL
          childImageSharp {
            fixed(base64Width: 750) {
              base64
            } 
          }
        }
      }
    }
  `)
  return (
    <section
      sx={{
        pt: 6,
        pb: 5,
        backgroundColor: '#F2F0EE',
        backgroundImage: `url('${data.allFile.nodes[0].childImageSharp.fixed.base64}')`,
        backgroundPositionY: '150px',
        backgroundPositionX: ['0vw', '20vw', '30vw', '40vw'],
        backgroundSize: '750px',
        backgroundRepeat: 'no-repeat',
      }}>
      <Container
        sx={{
          px: 4
        }}>
        <Flex
          sx={{
            mx: -4,
            mb: 4,
            flexWrap: 'wrap',
          }}>
          <Box
            sx={{
              px: 4,
              width: ['full',]
            }}>
            <Styled.h1
              sx={{
                fontSize: 8,
                color: 'primary',
                mt: 6
              }}>
              {title}
            </Styled.h1>
            <p>{subtitle}</p>
          </Box>
        </Flex>
        <Flex
          sx={{
            mx: -4,
            mb: '-7em',
            flexWrap: 'wrap'
          }}>
          <Box
            sx={{
              position: 'relative',
              px: 4,
              width: ['full']
            }}>
            <SearchForm />
            { filteredKnowledgeBase.length > 0 && (
              <KbList
                items={filteredKnowledgeBase} />
            )}
          </Box>
        </Flex>
      </Container>
    </section>
  )
}

const filterKnowledgeBase = (kb, search) => {
  if(!search.length){
    return []
  } 

  return kb.filter(({ title }) => title.toLowerCase().includes(search))
}

const mapStateToProps = state => ({
  filteredKnowledgeBase: filterKnowledgeBase(state.knowledge.allKnowledgeBase, state.knowledge.search)
})

export default connect(mapStateToProps)(KbHeader)