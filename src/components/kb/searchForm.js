/** @jsx jsx */
import { jsx, Flex, Styled, Button } from "theme-ui"
import { connect } from "react-redux"
import { SET_SEARCH } from "~/state/actions/knowledge"

const KbSearchForm = ({ search }) => {
  return (
    <Flex
      sx={{
        alignItems: 'center',
        justifyContent: 'center',
        px: 5,
        py: 3,
        bg: 'white',
        border: theme => `1px solid ${theme.colors.gray[2]}`,
        borderRadius: 'full',
        boxShadow: 'xl'
      }}
      as='form'
      onSubmit={e => {
        e.preventDefault()
      }}>
      <Styled.h3
        sx={{
          color: 'primay',
          m: 0,
          whiteSpace: 'nowrap'
        }}>How can we help?</Styled.h3>
      <input
        onChange={ e => {
          if(e.target.value.length < 3){
            search('')
          }else{
            search(e.target.value.toLocaleLowerCase())
          } 
        }}
        sx={{
          flexGrow: 1,
          px: 3,
          py: 1,
          fontSize: 3,
          border: 0,
          lineHeight: 'loose',
          outline: 'none',
          color: 'gray.6'}}
          placeholder={`start typing...`} />
      <Button
        sx={{
          border: 'none',
          bg: 'primary',
          fontWeight: 'bold',
          fontSize: 3,
          color: 'white',
          px: 4,
          py: 2,
          cursor: 'pointer',
        }}
        type="submit"
        value="submit">Search</Button>
    </Flex>
  )
}

const mapActionsToProps = dispatch => ({
  search: (value) => { dispatch({ type: SET_SEARCH, payload: value })}
})

export default connect(null, mapActionsToProps)(KbSearchForm)