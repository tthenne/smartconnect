/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { Link } from "gatsby"

export default ({ category, active = null }) => {
  return (
     <Flex
       sx={{
         flexDirection: 'column',
         mb: 4
       }}>
       <Box>
         <Styled.h5>
           { category.name }
         </Styled.h5>
       </Box>
       <Flex
         as="ul"
         sx={{
           listStyle: 'none',
           flexDirection: 'column',
           m: 0,
           mt: 1,
           p: 0,
           color: 'gray.5',
           fontSize: 1
         }}>
         { category.knowledgeBaseArticles.nodes.map(({ slug, title}) => (
           <li
             sx={{
               my: 1
             }}
             key={slug}>
            { slug === active ? (
              <span sx={{ color: 'gray.6', fontWeight: 700 }}>{ title }</span>
            ) : (
             <Link
               to={`/knowledge-base/${slug}`}>
               {title}
             </Link>
            )}
           </li>
         ))}   
       </Flex>
     </Flex>
  )
}