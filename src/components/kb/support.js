/** @jsx jsx */
import { jsx, Flex, Box, Styled, Button } from "theme-ui"
import EmailIcon from "~/images/kb/email.inline.svg"
import TicketIcon from "~/images/kb/ticket.inline.svg"
import ChatIcon from "~/images/kb/chat.inline.svg"
import CallIcon from "~/images/kb/call.inline.svg"

export default () => (
  <Flex
    sx={{
      mt: 6,
      mx: -2,
      flexWrap: 'wrap'
    }}>
    <Flex
      sx={{
        flexDirection: 'column',
        px: 2,
        width: ['full', '1/2']
      }}>

      <Flex
        sx={{
          flexWrap: 'wrap',
          alignItems: 'center',
          mb: 3,
          p: 4,
          bg: 'gray.2'
        }}>
        <Flex
          sx={{
            flexDirection: 'column',
            width: ['full', '2/3']
          }}>
          <Box>
            <small>Support</small>
          </Box>
          <Box>
            <Styled.h3
              sx={{
                mb: 4,
                mt: 3
              }}>Email an Expert</Styled.h3>
          </Box>
          <Box>
            <Button
              variant="inverse"
              sx={{
                color: 'primary',
                borderColor: 'primary'
              }}>Start a Ticket</Button>
          </Box>
        </Flex>
        <Box
          sx={{
            width: ['full', '1/3'],
            textAlign: 'right'
          }}>
          <EmailIcon />
        </Box>
      </Flex>
      <Flex
        sx={{
          flexWrap: 'wrap',
          alignItems: 'center',
          p: 4,
          bg: 'gray.2'
        }}>
        <Flex
          sx={{
            flexDirection: 'column',
            width: ['full', '2/3']
          }}>
          <Box>
            <small>Support</small>
          </Box>
          <Box>
            <Styled.h3
              sx={{
                mb: 4,
                mt: 3
              }}>Chat with Us</Styled.h3>
          </Box>
          <Box>
            <Button
              variant="inverse"
              sx={{
                color: 'primary',
                borderColor: 'primary'
              }}>Start a Ticket</Button>
          </Box>
        </Flex>
        <Box
          sx={{
            width: ['full', '1/3'],
            textAlign: 'right'
          }}>
          <ChatIcon />
        </Box>
      </Flex>
      
    </Flex>
    <Flex
      sx={{
        flexDirection: 'column',
        px: 2,
        width: ['full', '1/2']
      }}>
      
      <Flex
        sx={{
          flexWrap: 'wrap',
          alignItems: 'center',
          mb: 3,
          p: 4,
          bg: 'gray.2'
        }}>
        <Flex
          sx={{
            flexDirection: 'column',
            width: ['full', '2/3']
          }}>
          <Box>
            <small>Support</small>
          </Box>
          <Box>
            <Styled.h3
              sx={{
                mb: 4,
                mt: 3
              }}>Submit a Ticket</Styled.h3>
          </Box>
          <Box>
            <Button
              variant="inverse"
              sx={{
                color: 'primary',
                borderColor: 'primary'
              }}>Start a Ticket</Button>
          </Box>
        </Flex>
        <Box
          sx={{
            width: ['full', '1/3'],
            textAlign: 'right'
          }}>
          <TicketIcon />
        </Box>
      </Flex>
      <Flex
        sx={{
          flexWrap: 'wrap',
          alignItems: 'center',
          p: 4,
          bg: 'gray.2'
        }}>
        <Flex
          sx={{
            flexDirection: 'column',
            width: ['full', '2/3']
          }}>
          <Box>
            <small>Support</small>
          </Box>
          <Box>
            <Styled.h3
              sx={{
                mb: 4,
                mt: 3
              }}>Call Us</Styled.h3>
          </Box>
          <Box>
            <Button
              variant="inverse"
              sx={{
                color: 'primary',
                borderColor: 'primary'
              }}>Start a Ticket</Button>
          </Box>
        </Flex>
        <Box
          sx={{
            width: ['full', '1/3'],
            textAlign: 'right'
          }}>
          <CallIcon />
        </Box>
      </Flex>

    </Flex>
  </Flex>
)