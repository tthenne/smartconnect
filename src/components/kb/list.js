/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui"
import { navigate } from "gatsby"
import { connect } from "react-redux"
import { SET_SEARCH } from "~/state/actions/knowledge"

const KbList = ({ search, items }) => (
  <Flex
    sx={{
      position: 'absolute',
      flexDirection: 'column',
      width: 'full',
      left: 0,
      bg: 'white',
      p: 4,
      mt: -1,
      zIndex: 9999,
      borderRadius: 15,
      boxShadow: 'xl'
    }}>
    { items.map((kb, i) => (
      <Box
        key={kb.databaseId}
        sx={{
          pb: 2,
          mt: 2,
        }}>
          <a
            onClick={e => {
              e.preventDefault()
              //clear the search
              search('')
              navigate(`/knowledge-base/${kb.slug}`)
            }} 
            href={`/knowledge-base/${kb.slug}`}>
            { kb.title }
          </a>
      </Box>
    ))}
  </Flex>
)

const mapActionsToProps = dispatch => ({
  search: (value) => { dispatch({ type: SET_SEARCH, payload: value })}
})

export default connect(null, mapActionsToProps)(KbList)