/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import Img from "gatsby-image"

export default ({ download, last = false }) => {
  return (
    <Flex
      sx={{
        flexDirection: 'column',
        height: 'full',
        position: 'relative',
        borderStyle: 'solid',
        borderColor: 'gray.3',
        borderWidth: `1px ${last ? '0px' : '1px'} 1px 0px` 
      }}>
      <div
        sx={{
          position: 'absolute',
          top: 3,
          left: 4,
          bg: 'lightgreen',
          color: 'white',
          fontSize: 0,
          px: 3,
          py: 1,
          borderRadius: 'full',
          zIndex: 2
        }}>Downloads</div>
      <Img fluid={download.featuredImage.node.localFile.childImageSharp.fluid} />
      <Box
        sx={{
          p: 4
        }}>
        <Styled.h5
          sx={{
            color: 'primary'
          }}>{ download.title }</Styled.h5>
      </Box>
    </Flex>
  )
}