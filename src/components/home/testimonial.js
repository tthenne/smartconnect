/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui"
import Img from "gatsby-image"

import { FaQuoteLeft } from "react-icons/fa"

export default ({ name = '', title = '', quote = '', profile = {} }) => {
  return (
    <Flex
      sx={{
        flexWrap: 'wrap',
        borderStyle: 'solid',
        borderWidth: 16,
        borderColor: 'lightblue',
        pt: 3,
        pl: [3, 4],
        pr: [3, 0]
      }}>
      <Box
        sx={{
          width: ['full', '7/12'],
          p: 3,
          position: 'relative'
        }}>
        <FaQuoteLeft size="4em" color="#eee" style={{ position: 'absolute' }} />
        <div
          sx={{
            position: 'relative',
            fontSize: '18px',
            zIndex: 2
          }}
          dangerouslySetInnerHTML={{
            __html: quote
          }} />
          <p
            sx={{
              color: 'gray.6',
              fontSize: 0
            }}>
            <span>{ name }</span>
            { `, ` }
            <span>{ title }</span>
          </p>
      </Box>
      <Box
        sx={{
          width: ['full', '5/12'],
        }}>
        <Img
          sx={{
            mt: [2, -6],
            mx: -4
          }}
          fluid={profile.localFile.childImageSharp.fluid} alt={name} />
      </Box>
    </Flex>
  )
}