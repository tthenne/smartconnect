/** @jsx jsx */
import { jsx, Flex, Box, Styled, Button } from "theme-ui"

import Checkmark from "../../images/icons/checkmark.inline.svg"

export default ({ table }) => {
  return (
    <Flex
      sx={{
        flexDirection: 'column',
        p: 4,
        mb: 5,
        bg: 'gray.2',
        borderWidth: '6px 0 0',
        borderStyle: 'solid',
        borderColor: 'transparent',
        borderImage: theme => `linear-gradient(to right, rgba(0,0,0,0) 5%, ${theme.colors.lightblue} 5%, ${theme.colors.lightblue} 20%, rgba(0,0,0,0) 20%) 1`,
        borderRadius: 10
      }}>
        <Flex
          sx={{
            mb: 3,
            alignItems: 'center'
          }}>
          <Box
            sx={{
              flexGrow: 1
            }}>
            <Styled.h1
              sx={{
                color: 'primary',
                m: 0
              }}>{ table.title }</Styled.h1>
          </Box>
          <Box>
            <Button
              variant="inverse">Compare</Button>
          </Box>
        </Flex>
        <Flex
          sx={{
            listStyle: 'none',
            p: 0,
            m: 0,
            flexDirection: 'column'
          }}
          as="ul">
          { table.lineitems.map(({item}, i) => (
            <Flex
              as="li"
              sx={{
                alignItems: 'center',
                mb: 2
              }}
              key={i}>
              <Checkmark />
              <span
                sx={{
                  ml: 3
                }}>
                  {item}
              </span>          
            </Flex>
          ))}    
        </Flex>
      </Flex>

  )
}