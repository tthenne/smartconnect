/** @jsx jsx */
import { jsx, Flex, Styled, Button, Select } from "theme-ui"

export default () => {
  return (
    <Flex
      sx={{
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        px: 5,
        mx: 2,
        py: 3,
        bg: 'white',
        border: theme => `1px solid ${theme.colors.gray[4]}`,
        borderRadius: 'full'
      }}
      as='form'
      onSubmit={e => {
        e.preventDefault()
      }}>
      <Styled.h3
        sx={{
          color: 'primay',
          m: 0
        }}>I need to integrate</Styled.h3>
      <Select
        mx={4}
        sx={{
          mr: 4
        }}>
        <option>Microsoft Business Central</option>
        <option>Boop</option>
        <option>Blip</option>
      </Select>
      <Styled.h3
        sx={{
          color: 'primay',
          m: 0
        }}>with</Styled.h3>
      <Select
        mx={4}
        sx={{
          mr: 4
        }}>
        <option>FTP Server</option>
        <option>Boop</option>
        <option>Blip</option>
      </Select>
      <Button
        sx={{
          border: 'none',
          bg: 'gray.2',
          fontWeight: 'bold',
          fontSize: 3,
          color: 'primary',
          px: 4,
          py: 2,
          cursor: 'pointer',
          '&:hover': {
            bg: 'lightblue',
            color: 'white'
          }
        }}
        type="submit"
        value="submit">Search</Button>
    </Flex>
  )
}