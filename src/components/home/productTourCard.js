/** @jsx jsx */
import { jsx, Styled } from "theme-ui"
import { Link } from "gatsby"

export default ({ section }) => {
  return (
    <Link
      to={`/product-tour/#${section.slug}`}
      sx={{
        display: 'flex',
        flexDirection: 'column',
        width: ['full', '1/3', '1/4'],
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: 'transparent',
        borderRadius: 5,
        p: 4,
        mb: 3,
        '&:hover': {
          textDecoration: 'none',
          borderColor: 'gray.2'
        }
      }}>
      {section.icon && (
        <img
          sx={{
            mb: 3,
            height: '25px'
          }}
          src={section.icon.localFile.publicURL} alt={section.title} />
      )}
      <Styled.h3
        sx={{
          maxWidth: ['full', '3/4', '2/3'],
          color: 'primary',
          lineHeight: 1.1,
          mb: 4
        }}>{section.title}</Styled.h3>
      <Styled.h5
        sx={{
          color: 'lightblue',
        }}>{section.subtitle}</Styled.h5>
      <p
        sx={{
          color: 'text'
        }}>{ section.excerpt.slice(0, 150) + '...' }</p>
      <small
        sx={{
          mt: 'auto',
          fontSize: 0,
          fontWeight: 'bold',
          color: 'primary'
        }}>Learn More &rsaquo;</small>
    </Link>
  )
}