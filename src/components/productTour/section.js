/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled } from "theme-ui"
import Img from "gatsby-image"
import Tabs from "../tabs"

const Section = ({ section, bg = 'white', even = false }) => {
  return (
    <section
      id={section.slug}
      sx={{
        bg,
        py: 6
      }}>
      <Container
        sx={{
          px: 4
        }}>
        <Flex
          sx={{
            mx: -4,
            flexWrap: 'wrap'
          }}>
          <Flex
            sx={{
              flexDirection: 'column',
              width: ['full', '1/2'],
              px: 4,
              order: even ? 2 : 0,
              alignItems: even ? 'flex-end' : 'flex-start'
            }}>
            { section.icon && (
              <img
                sx={{
                  mb: 4
                }}
                src={section.icon.localFile.publicURL} alt={section.title} />
            )}
            <Styled.h5
              sx={{
                color: 'secondary',
                textTransform: 'uppercase',
                mb: 3
              }}>{section.subtitle}</Styled.h5>
            <Styled.h1
              sx={{
                maxWidth: ['full', '3/4', '2/3'],
                color: 'primary',
                fontSize: 7,
                lineHeight: 1.1,
                textAlign: even ? 'right' : 'left',
                mb: 4
              }}>{section.title}</Styled.h1>
            <Tabs
              tabs={section.contents} />
          </Flex>
          <Flex
            sx={{
              width: ['full', '1/2'],
              px: 4
            }}>
              { section.screenshot && (
                <Box
                  sx={{
                    mt: 5,
                    width: 'full'
                  }}>
                  <Img fluid={section.screenshot.localFile.childImageSharp.fluid} alt={section.title} />
                </Box>
              )}
          </Flex>
        </Flex>
      </Container>
    </section>
  )
}

export default Section