/** @jsx jsx */
import { jsx, Flex, Box, Styled, Button } from "theme-ui"

export default ({ children, orientation = 0, title = '', subtitle = '', icon = null, description = '', cta = ''}) => {
  return (
    <Flex
      sx={{
        flexDirection: orientation ? 'column' : 'row',
        justifyContent: orientation ? 'center' : 'flex-start',
        alignItems: 'center',
        bg: 'gray.2',
        borderRadius: 10,
        p: 4,
      }}>
      <Box
        sx={{
          order: 1,
          textAlign: orientation ? 'center' : 'left'
        }}>
        <Styled.h6
          sx={{
            fontSize: 0,
            color: 'primary',
            textTransform: 'uppercase'
          }}>{ subtitle }</Styled.h6>
        <Styled.h3
          sx={{
            color: 'primary',
            mt: 2,
            mb: 3
          }}>{ title }</Styled.h3>
          { children }
        <Button
          sx={{
            mt: 4
          }}
          variant="inverse">{ cta }</Button>
      </Box>
      <Box
        sx={{
          flexShrink: orientation ? 'initial' : 1,
          order: orientation ? 0 : 2,
          pl: orientation ? 0 : 5,
          mb: orientation ? 4 : 0,
          textAlign: 'center'
        }}>
          <img
            sx={{
              maxWidth: orientation ? '60%' : '85px'
            }}
            src={icon}
            alt={title} />
      </Box>
    </Flex> 
  )
}