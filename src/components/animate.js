import React from 'react';
import { useInView } from 'react-intersection-observer';

export default ({ 
  children,
  threshold = 0,
  delay = 0,
  rootMargin = '0px 0px 0px 0px',
  triggerOnce = false,
  animation = 'animate__fadeIn',
  animationDelay = 0,
  speed = 0,
  repeat = 0
}) => {
  const [ref, inView] = useInView({
    threshold,
    rootMargin,
    triggerOnce,
    ...(delay && { delay })
  });

  return (
    <div
      ref={ref}
      style={{
        opacity: inView ? 1 : 0
      }}
      className={`animate__animated${inView ? ' ' + animation : ''}${animationDelay ? ' animate__delay-' + animationDelay : ''}${speed ? ' ' + speed : ''}${repeat ? ' animate__repeat-' + repeat : ''}`}>
      {children}
    </div>
  )
}