/** @jsx jsx */
import { jsx } from "theme-ui"
import { Helmet } from "react-helmet"
import PropTypes from "prop-types"

import Header from "~/components/header"
import Footer from "~/components/footer"

const Layout = ({ children }) => {

  return (
    <div>
      <Helmet>
        <link rel="icon" href={'/favicon.ico'} />
        <link rel="icon" type="image/png" sizes="16x16" href={'/favicon-16x16.png'} />
        <link rel="icon" type="image/png" sizes="32x32" href={'/favicon-32x32.png'} />
        <link rel="apple-touch-icon" sizes="180x180" href={'/apple-touch-icon.png'} />
      </Helmet>
      <Header />
      <main>{children}</main>
      <Footer /> 
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
