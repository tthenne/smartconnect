/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { Fragment } from "react"
import IncludedIcon from "~/images/icons/included.inline.svg"

const TableRow = ({row, even}) => (
  <Flex
    sx={{
      flexWrap: 'wrap',
      alignItems: 'center',
      p: 3,
      mx: -4,
      bg: even && 'gray.1'
    }}>
    { row.header ? (
      <Box
        sx={{
          px: 4,
          width: ['full']
        }}>
        <Styled.h4>{row.feature}</Styled.h4>
      </Box>
    ) : (
        <Fragment>
          <Box
            sx={{
              px: 4,
              width: ['full', '1/2'],
              mb: [3, 0]
            }}>
            <span sx={{ fontSize: [1, 2] }}>{row.feature}</span>
          </Box>
          <Flex
            sx={{
              alignItems: 'flex-start',
              px: 4,
              width: ['full', '1/4'],
              mb: [3, 0]
            }}>
            <Styled.h6
              sx={{
                display: ['inline-block', 'none'],
                mr: [3, 0]
              }}>SmartConnect</Styled.h6>
            {row.smartconnect && <IncludedIcon sx={{ width: 5 }} />}
          </Flex>
          <Flex
            sx={{
              alignItems: 'flex-start',
              px: 4,
              width: ['full', '1/4']
            }}>
            <Styled.h6
              sx={{
                display: ['inline-block', 'none'],
                mr: [3, 0]
              }}>On-Premise</Styled.h6>
            {row.onpremise && <IncludedIcon sx={{ width: 5 }} />}
          </Flex>
        </Fragment>
      )}
  </Flex>
)

export default TableRow