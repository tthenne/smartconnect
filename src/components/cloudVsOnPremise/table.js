/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import TableRow from "~/components/cloudVsOnPremise/tableRow.js"

const Table = ({ table }) => (
  <Box
    sx={{
      px: 4,
      width: ['full']
    }}>
    <Flex
      sx={{
        justifyContent: 'flex-end',
        flexWrap: 'wrap',
        mx: -4,
        pb: 5,
      }}>
      <Box
        sx={{
          px: 4,
          width: ['full', '1/4']
        }}>
        <Styled.h4>SmartConnect</Styled.h4>
      </Box>
      <Box
        sx={{
          px: 4,
          width: ['full', '1/4']
        }}>
        <Styled.h4>On-Premise</Styled.h4>
      </Box>
    </Flex>
    <Flex
      sx={{
        flexDirection: 'column'
      }}>
      {table.map((row, i) => (
        <TableRow
          key={row.slug}
          even={i % 2 === 0}
          row={row} /> 
      ))}
    </Flex>
  </Box>
)

export default Table