/** @jsx jsx */
import { jsx, Container, Flex, Box } from "theme-ui"
import { connect } from "react-redux"
import { MobileProvider } from "../hooks/useMobile"
import { useWindowScroll } from "../hooks/useWindowScroll"
import Logo from "./logo"
import HamburgerMenu from "./hamburger"
import Menu from "./menu"

const Header = ({ theme }) => {
  const inverse = useWindowScroll()
  const color = inverse ? 'primary' : theme === 'light' ? 'white' : 'primary'
  return (
    <MobileProvider>
      <header
        sx={{
          position: 'fixed',
          top: 0,
          bg: inverse ? 'white' : 'transparent',
          width: 'full',
          py: 3,
          zIndex: 99,
        }}>
        {/** Top Bar */}
        <div
          sx={{
            display: ['none', 'block'],
            mb: 2
          }}>
          <Container
            sx={{
              px: 4
            }}>
            <Flex
              sx={{
                mx: -4,
                justifyContent: 'space-between',
                alignItems: 'center'
              }}>
              <Box
                sx={{
                  px: 4,
                }}>
                <span
                  sx={{
                    fontFamily: 'heading',
                    color: color,
                  }}>ERP & CRM Integration, Accelerated.</span>
              </Box>
              <Flex
                sx={{
                  px: 4,
                  alignItems: 'center'
                }}>
                <a
                  href="/#"
                  sx={{
                    fontSize: 1,
                    mx: 2,
                    color: color 
                  }}>
                  See a Demo
              </a>
                <a
                  href="/#"
                  sx={{
                    fontSize: 1,
                    mx: 3,
                    color: color 
                  }}>
                  Sign In
              </a>
              </Flex>
            </Flex>
          </Container>
        </div>
        <div
          sx={{
            display: ['block', 'none']
          }}>
          <Container
            sx={{
              px: 4
            }}>
            <Flex
              sx={{
                mx: -4,
              }}>
              <Logo color={color} />
            </Flex>
          </Container>
        </div>
        <Menu color={color} />
        <HamburgerMenu color={color} />
      </header>
    </MobileProvider>
  )
}

const mapStateToProps = state => ({
  theme: state.header.theme
})

export default connect(mapStateToProps)(Header)
