import React from 'react';
import './switch.css';
import { classList } from '~/helpers';


const Switch = ({ options: [first, second], active, change }) => {
  return (
    <>
      <div className="tabs-menu">
        <span
          role="presentation"
          onClick={e => {
            change(first)
          }}
          className={classList({
            'pricing-tab': true,
            'active': active === first
          })}>
          <div>{ first }</div>
        </span>
        <span
          role="presentation"
          onClick={e => {
            change(second)
          }}
          className={classList({
            'pricing-tab': true,
            'active': active === second 
          })}>
          <div>{ second }</div>
        </span>
      </div>
    </>
  );
};

export default Switch