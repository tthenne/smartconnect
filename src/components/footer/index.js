/** @jsx jsx */
import { jsx, Flex, Box, Styled, Container, Button } from "theme-ui"
import { Link, useStaticQuery, graphql } from "gatsby"
import { AiFillTwitterSquare, AiFillLinkedin } from "react-icons/ai"


import TemplateIcon from "~/images/icons/templateIcon.inline.svg"
import MicrosoftIcon from "~/images/icons/microsoft.inline.svg"
import SalesforceIcon from "~/images/icons/salesforce.inline.svg"
import ZendeskIcon from "~/images/icons/zendesk.inline.svg"
import NetsuiteIcon from "~/images/icons/netsuite.inline.svg"
import MagentoIcon from "~/images/icons/magento.inline.svg"
import ShopifyIcon from "~/images/icons/shopify.inline.svg"

export default () => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
        }
      }
      tour: wpPage(slug: {eq: "product-tour"}) {
        productTour {
          sections {
            slug
            title
          }
        }
      }
    }
  `)

  const connections = [
    {
      id: 1,
      title: 'Microsoft Dynamics 365',
      link: '/',
      icon: MicrosoftIcon,
    },
    {
      id: 2,
      title: 'Microsoft Dynamics NAV',
      link: '/',
      icon: MicrosoftIcon,
    },
    {
      id: 3,
      title: 'Micorsoft Dynamics GP',
      link: '/',
      icon: MicrosoftIcon,
    },
    {
      id: 4,
      title: 'Salesforce',
      link: '/',
      icon: SalesforceIcon,
    },
    {
      id: 5,
      title: 'Zendesk',
      link: '/',
      icon: ZendeskIcon,
    },
    {
      id: 6,
      title: 'Netsuite',
      link: '/',
      icon: NetsuiteIcon,
    },
    {
      id: 7,
      title: 'Magento',
      link: '/',
      icon: MagentoIcon,
    },
    {
      id: 8,
      title: 'Shopify',
      link: '/',
      icon: ShopifyIcon,
    },
  ];

  const templates = [
    {
      id: 1,
      title: 'Dynamics 365 - Dynamics GP',
      link: '/'
    },
    {
      id: 2,
      title: 'Salesforce - Dynamics GP',
      link: '/'
    },
    {
      id: 3,
      title: 'Dynamics 365 BC - Dynamics',
      link: '/'
    },
    {
      id: 4,
      title: 'Dynamics 365 BC - Salesforce',
      link: '/'
    },
    {
      id: 5,
      title: 'Dynamics NAV - Salesforce',
      link: '/'
    },
    {
      id: 6,
      title: 'SAP Concur – Dynamics 365 BC',
      link: '/'
    },
    {
      id: 7,
      title: 'SAP Concur – Dynamics GP',
      link: '/'
    },
    {
      id: 9,
      title: 'Zendesk – Dynamics 365',
      link: '/'
    },
    {
      id: 10,
      title: 'Zendesk – Salesforce',
      link: '/'
    },
  ];

  return (
    <footer
      sx={{
        py: 5,
        mt: 4,
        bg: 'gray.2'
      }}>
      <Container
        sx={{
          px: 4
        }}>
        <Flex
          sx={{
            mx: -4,
            flexWrap: 'wrap'
          }}>
          <Box
            sx={{
              px: 4,
              width: ['full', '1/4'],
              mb: [4, 0]
            }}>
            <Styled.h6
              sx={{
                color: 'gray.7',
                textTransform: 'uppercase',
                mb: 4
              }}>Product</Styled.h6>
            <Flex
              as="ul"
              sx={{
                flexDirection: 'column',
                mx: 0,
                p: 0,
                listStyle: 'none'
              }}>
              {
                data.tour.productTour.sections.map(section => (
                  <li
                    sx={{
                      my: 1
                    }}
                    key={section.slug}>
                    <Link
                      sx={{
                        color: 'gray.7',
                        fontSize: 1
                      }}
                      to={`/product-tour/#${section.slug}`}>
                      {section.title}
                    </Link>
                  </li>
                ))
              }
            </Flex>
          </Box>
          <Box
            sx={{
              px: 4,
              width: ['full', '1/4'],
              mb: [4, 0]
            }}>
            <Styled.h6
              sx={{
                color: 'gray.7',
                textTransform: 'uppercase',
                mb: 4
              }}>Connections</Styled.h6>
            <Flex
              as="ul"
              sx={{
                flexDirection: 'column',
                mx: 0,
                my: 3,
                p: 0,
                listStyle: 'none'
              }}>
              {
                connections.map(({ id, title, link, icon: Icon }) => (
                  <Flex
                    sx={{
                      flexDirection: 'column',
                      my: 1,
                    }}
                    as="li"
                    key={id}>
                    <Flex
                      sx={{ alignItems: 'center' }}>
                      <Icon sx={{ width: '25px' }} />
                      <Link
                        sx={{
                          color: 'gray.7',
                          fontSize: 1,
                          ml: 2
                        }}
                        to={`${link}`}>
                        {title}
                      </Link>
                    </Flex>
                  </Flex>
                ))
              }
            </Flex>
          </Box>
          <Box
            sx={{
              px: 4,
              width: ['full', '1/4'],
              mb: [4, 0]
            }}>
            <Styled.h6
              sx={{
                color: 'gray.7',
                textTransform: 'uppercase',
                mb: 4
              }}>Templates</Styled.h6>
            <Flex
              as="ul"
              sx={{
                flexDirection: 'column',
                mx: 0,
                p: 0,
                listStyle: 'none'
              }}>
              {
                templates.map(template => (
                  <Flex
                    sx={{
                      alignItems: 'center',
                      my: 1 
                    }}
                    as="li"
                    key={template.id}>
                    <TemplateIcon />
                    <Link
                      sx={{
                        color: 'gray.6',
                        fontSize: 1,
                        ml: 2
                      }}
                      to={`${template.link}`}>
                      {template.title}
                    </Link>
                  </Flex>
                ))
              }
            </Flex>
          </Box>
          <Flex
            sx={{
              px: 4,
              flexDirection: 'column',
              width: ['full', '1/4']
            }}>
            <Styled.h6
              sx={{
                color: 'gray.7',
                textTransform: 'uppercase',
                mb: 4
              }}>Engage</Styled.h6>
            <Flex
              sx={{
                flexGrow: 1,
                flexDirection: 'column',
                justifyContent: 'space-between'
              }}>
              <Flex
                sx={{
                  flexDirection: 'column'
                }}>
                <Flex
                  sx={{
                    flexWrap: 'wrap'
                  }}>
                  <Box
                    sx={{
                      px: 1,
                      width: ['full', '1/2']
                    }}>
                    <Button
                      sx={{
                        width: 'full',
                        border: theme => `1px solid ${theme.colors.secondary}`,
                        fontSize: 0
                      }}
                      variant="secondary">See a Demo</Button>
                  </Box>
                  <Box
                    sx={{
                      px: 1,
                      mt: [2, 0],
                      width: ['full', '1/2']
                    }}>
                    <Button
                      sx={{
                        width: 'full',
                        fontSize: 0
                      }}
                      variant="blue">Sign In</Button>
                  </Box>
                </Flex>
                <Box
                  sx={{
                    px: 1,
                    mt: 2,
                    width: 'full'
                  }}>
                  <Button
                    sx={{
                      width: 'full',
                      fontSize: 0
                    }}
                    variant="inverse">Get Started</Button>
                </Box>
              </Flex>
              <Box
                sx={{
                  mt: 4
                }}>
                <p
                  sx={{
                    fontSize: 0
                  }}>SmartConnect is designed with specialty & deep experience in the core technologies we focus on.</p>
                <p
                  sx={{
                    fontSize: 0
                  }}>iPAAS & on-premise integration tools that let you connect your cloud and on-premise applications.</p>
              </Box>
            </Flex>
          </Flex>
        </Flex>
        <Flex
          sx={{
            mx: -4,
            mt: 5,
            flexWrap: 'wrap',
            alignItems: 'center',
            fontSize: 0
          }}>
          <Box
            sx={{
              px: 4,
              width: ['full', '1/3'],
              textAlign: ['center', 'left'],
              mb: [3, 0]
            }}>
            <span>&copy;</span>
            {` `}
            <span>{new Date().getFullYear()}</span>
            {` `}
            <span>{data.site.siteMetadata.title}</span>
            <span sx={{ mx: 1 }}>|</span>
            <a href="https://eonesolutions.com">eOneSolutions</a>
          </Box>
          <Flex
            sx={{
              px: 4,
              width: ['full', '1/3'],
              justifyContent: 'center',
              mb: [3, 0]
            }}>
            <AiFillTwitterSquare size="1.35rem" />
            <span sx={{ mx: 2 }}></span>
            <AiFillLinkedin size="1.35rem" />
          </Flex>
          <Flex
            sx={{
              px: 4,
              width: ['full', '1/3'],
              justifyContent: ['center', 'flex-end']
            }}>
            <a href="/#">Privacy Policy</a>
            <a sx={{ mx: 3 }} href="/#">Terms of Service</a>
            <a href="/#">Language</a>
          </Flex>
        </Flex>
      </Container>
    </footer>
  )
}