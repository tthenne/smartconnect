/** @jsx jsx */
import { jsx, Flex, Box, Styled, Container, Button } from "theme-ui"

export default () => {
  return (
    <section
      sx={{
        py: 5
      }}>
      <Container
        sx={{
          px: 5
        }}>
        <Flex
          sx={{
            mx: -5,
            flexWrap: 'wrap'
          }}>
          <Flex
            sx={{
              p: 5,
              bg: '#3D4657',
              flexDirection: 'column',
              width: ['full', '1/3'],
              color: 'white'
            }}>
            <Box><small>Take a peak.</small>
            </Box>
            <Box
              sx={{
                my: 3
              }}>
              <Styled.h3
                sx={{
                  m: 0,
                  color: 'white'
                }}>Product Tour</Styled.h3>
            </Box>
            <Box
              sx={{
                mt: 'auto'
              }}>
              <Button
                sx={{
                  px: 3,
                  py: 2,
                  bg: 'transparent',
                  borderRadius: 'full',
                  border: theme => `2px solid ${theme.colors.white}`
                }}>Get Started</Button>
            </Box>
          </Flex>
          <Flex
            sx={{
              p: 5,
              bg: '#4C566A',
              flexDirection: 'column',
              width: ['full', '1/3'],
              color: 'white'
            }}>
            <Box><small>Kick things off.</small></Box>
            <Box
              sx={{
                my: 3
              }}>
              <Styled.h3
                sx={{
                  m: 0,
                  color: 'white'
                }}>Start a Trial</Styled.h3>
            </Box>
            <Box
              sx={{
                mt: 'auto'
              }}>
              <Button
                sx={{
                  px: 3,
                  py: 2,
                  bg: 'transparent',
                  borderRadius: 'full',
                  border: theme => `2px solid ${theme.colors.white}`
                }}>Get Started</Button>
            </Box>
          </Flex>
          <Flex
            sx={{
              p: 5,
              bg: '#58657E',
              flexDirection: 'column',
              width: ['full', '1/3'],
              color: 'white'
            }}>
            <Box><small>Engage.</small></Box>
            <Box
              sx={{
                my: 3
              }}>
              <Styled.h3
                sx={{
                  m: 0,
                  color: 'white'
                }}>Get a Demo</Styled.h3>
            </Box>
            <Box
              sx={{
                mt: 'auto'
              }}>
              <Button
                sx={{
                  px: 3,
                  py: 2,
                  bg: 'transparent',
                  borderRadius: 'full',
                  border: theme => `2px solid ${theme.colors.white}`
                }}>Get Started</Button>
            </Box>
          </Flex>
        </Flex>
      </Container>
    </section>
  )
}