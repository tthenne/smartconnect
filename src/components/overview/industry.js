/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"

export default ({ industry }) => {
  return (
    <Flex
      sx={{
        flexDirection: 'column',
        alignItems: 'center'
      }}>
      <Box
        sx={{
          textAlign: 'center',
          mb: 5
        }}>
          <Styled.h2
            sx={{
              mb: 3
            }}>{ industry.name }</Styled.h2>
          <Styled.h5>{ industry.subtitle }</Styled.h5>
        </Box>
      <Flex
        sx={{
          mx: -4,
          justifyContent: 'center',
          flexWrap: 'wrap'
        }}>
          { industry.features.map((feature, i) => (
            <Box
              key={i}
              sx={{
                px: 5,
                width: ['full', '1/3']
              }}>
                <Styled.h5>{ feature.title }</Styled.h5>
                <p>{ feature.description }</p>
            </Box> 
          ))}
      </Flex>
    </Flex>
  )
}