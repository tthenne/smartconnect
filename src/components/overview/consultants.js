/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { useState, Fragment } from "react"
import FeatureCard from "~/components/overview/featureCard"

export default ({ who }) => {
  const [active, setActive] = useState(who.useCase[0])
  return (
    <Fragment>
      <Flex
        sx={{
          px: 4,
          width: ['full', '7/12'],
          justifyContent: 'flex-end'
        }}>
        <div
          sx={{
            flexShrink: 1,
            mt: '20rem',
            mr: -3
          }}>
          <FeatureCard
            active={active} /> 
        </div>
      </Flex>
      <Box
        sx={{
          flexDirection: 'column',
          px: 4,
          width: ['full', '5/12']
        }}>
        <Styled.h2>
          {who.jobTitle}
        </Styled.h2>
        <p>
          {who.description}
        </p>
        <Flex
          as="ul"
          sx={{
            flexDirection: 'column',
            listStyle: 'none',
            pl: 3,
          }}>
          {who.useCase.map(use => (
            <li
              role="presentation"
              onClick={e => {
                setActive(use)
              }}
              sx={{
                my: 1,
                cursor: 'pointer'
              }}
              key={use.slug}>
              <strong><u>{use.feature}</u></strong>
            </li>
          ))}
        </Flex>
      </Box>
    </Fragment>
  )
}