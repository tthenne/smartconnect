/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"

export default ({ active }) => {
  return (
    <Flex
      sx={{
        flexWrap: 'wrap',
        alignItems: 'center',
        maxWidth: 'lg',
        minHeight: 64,
        p: 4,
        bg: 'white',
        boxShadow: 'xl',
        borderRadius: 20
      }}>
      <Box
        sx={{
          px: 3,
          width: ['full', '5/12']
        }}>
        <Styled.h4
          sx={{
            mt: 3
          }}>{ active.feature }</Styled.h4>  
        <p sx={{
          fontSize: 0
        }}>{ active.content }</p>
      </Box>
      <Box
        sx={{
          px: 3,
          width: ['full', '7/12']
        }}>
        <img 
          sx={{
            maxWidth: 'full'
          }}
          src={active.thumbnail.localFile.publicURL} alt={active.feature} /> 
      </Box>
    </Flex>
  )
}