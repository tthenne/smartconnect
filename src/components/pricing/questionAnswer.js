/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { FiChevronDown } from "react-icons/fi"

const QuestionAnswer = ({ question, answer, slug, active, activate }) => {
  return (
    <Flex
      sx={{
        flexDirection: 'column',
        py: 4
      }}>
        <Flex
          role="presentation"
          onClick={ e => {
            activate(slug)
          }}
          sx={{
            justifyContent: 'space-between',
            cursor: 'pointer'
          }}>
          <Styled.h4
            sx={{
              textDecoration: 'underline',
            }}>
            { question }
          </Styled.h4>
          <FiChevronDown size="1.5em" />
        </Flex>
        { active && (
          <Box>
            <div dangerouslySetInnerHTML={{
              __html: answer
            }} />
          </Box>
        )}
      </Flex>
  )
}

export default QuestionAnswer