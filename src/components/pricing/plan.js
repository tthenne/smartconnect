/** @jsx jsx */
import { jsx, Flex, Box, Styled, useThemeUI } from "theme-ui"
import { Link } from "gatsby"

export default ({ plan, type = "subscription" }) => {
  const { theme } = useThemeUI()
  return (
    <Flex
      sx={{
        position: 'relative',
        height: 'full',
        flexDirection: 'column',
        borderWidth: 3,
        borderStyle: 'solid',
        borderColor: plan.highlight ? 'lightgreen' : 'gray.3',
        borderRadius: 10,
        py: 4,
        px: 5
      }}>
      { plan.highlight && (
        <div
          sx={{
            position: 'absolute',
            top: 0,
            right: 0,
            mt: 3,
            mr: 3,
            bg: 'lightgreen',
            color: 'white',
            borderRadius: 'full',
            px: 2,
            py: 1,
            fontSize: 0
          }}>Most Popular</div>
      )}
      <Box>
        <Styled.h3
          sx={{
            fontSize: 5,
            color: plan.highlight ? 'lightgreen' : 'primary'
          }}>
          {plan.title}
        </Styled.h3>
      </Box>
      <Box>
        <span
          sx={{
            color: 'primary',
            fontSize: 7,
            fontWeight: 'black',
            fontFamily: 'heading' 
          }}>${plan[type].price}</span> / <span>month</span>
      </Box>
      <Box>
        <div
          sx={{
            ul: {
              p: 0
            }
          }}
          dangerouslySetInnerHTML={{
            __html: plan[type].features
          }} />
      </Box>
      <Box
        sx={{
          pt: 4,
          mt: 'auto' 
        }}>
        <Link
          sx={{
            ...(plan.highlight ? theme.buttons.green : theme.buttons.primary),
            display: 'block',
            fontSize: 2,
            textAlign: 'center'
          }}
          to={plan[type].link}>
            Get Started
        </Link>
      </Box>
    </Flex>
  )
}