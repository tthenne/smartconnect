/** @jsx jsx */
import { jsx, Flex, Box, Styled, useThemeUI } from "theme-ui"
import { Link } from "gatsby"

export default ({ plan, type = "subscription", last = false }) => {
  const { theme } = useThemeUI()
  return (
    <Flex
      sx={{
        height: ['auto', 'full'],
        flexDirection: 'column',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: plan.highlight ? 'lightgreen' : 'gray.3',
        ...(plan.highlight && { borderBottomColor: 'gray.3' }),
        borderRadius: '6px 6px 0 0',
        py: 4,
        px: 5
      }}>
      <Box>
        <Styled.h5
          sx={{
            fontSize: 3,
            color: plan.highlight ? 'lightgreen' : 'primary'
          }}>
          {plan.title}
        </Styled.h5>
      </Box>
      <Box>
        <span
          sx={{
            color: 'primary',
            fontSize: 4,
            fontWeight: 'black',
            fontFamily: 'heading' 
          }}>${plan[type].price}</span> / <span>month</span>
      </Box>
      <Box
        sx={{
          mt: 4 
        }}>
        <Link
          sx={{
            ...(plan.highlight ? theme.buttons.green : theme.buttons.inverse),
            display: 'block',
            fontSize: 0,
            textAlign: 'center'
          }}
          to={plan[type].link}>
            Get Started
        </Link>
      </Box>
    </Flex>
  )
}