/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui"
import { Fragment } from "react"
import IncludedIcon from "~/images/icons/included.inline.svg"
import ExcludedIcon from "~/images/icons/excluded.inline.svg"

export default ({ row, index, last }) => {
  const plans = ["basic", "business", "premium"]
  return (
    <Flex
      sx={{
        alignItems: 'center',
        bg: index % 2 === 0 && !row.separator ? 'gray.1' : 'white',
      }}>
      <Box
        sx={{
          width: '1/3',
          pt: row.separator ? '25px' : '15px',
          pb: row.separator ? '5px' : '15px',
          px: 3,
          ...(row.separator && { borderBottom: '1px solid lightgray' })
        }}>
        <span
          sx={{
            fontWeight: row.separator ? 'bold' : 'light'
          }}>{row.feature}</span>
      </Box>
      <Flex
        sx={{
          width: '2/3',
          alignItems: 'center',
        }}>
        {plans.map(plan => (
          <Box
            key={plan}
            sx={{
              width: '1/3',
              height: 'full',
              p: 3,
              textAlign: 'center',
              borderStyle: 'solid',
              borderColor: 'lightgreen',
              borderWidth: plan === "business" ? '0 1px' : 0,
              borderBottomWidth: plan === "business" && last ? 1 : 0,
              borderRadius: last && '0 0 10px 10px' 
            }}>
            { !row.separator ? (
              <Fragment>
                {row[plan].text ? (
                  <span>{row[plan].text}</span>
                ) : (
                  <span>
                    {row[plan].included ? <IncludedIcon sx={{ width: 6 }} /> : <ExcludedIcon sx={{ width: 4 }} />}
                  </span>
                )}
              </Fragment>
            ) : <span >&nbsp;</span>}
            
          </Box>
        ))}
      </Flex>
    </Flex>
  )
}