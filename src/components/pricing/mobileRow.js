/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui"

import IncludedIcon from "~/images/icons/included.inline.svg"
import ExcludedIcon from "~/images/icons/excluded.inline.svg"

export default ({ row, plan, last }) => {
  return (
    <Flex
      sx={{
        display: ['flex', 'none'],
        flexDirection: 'column',
        alignItems: 'center',
        borderStyle: 'solid',
        borderColor: plan === "business" ? 'lightgreen' : 'gray.2',
        borderWidth: '0 1px',
        borderBottomWidth: last && 1,
        borderRadius: last && '0 0 10px 10px'
      }}>
      <Box
        sx={{
          width: 'full',
          p: 3,
          bg: row.separator ? 'gray.3' : 'gray.1',
          textAlign: 'center',
          ...(row.separator && { borderBottom: '1px solid lightgray' })
        }}>
        <span
          sx={{
            fontWeight: 'bold',
            fontSize: row.separator ? 3 : 1
          }}>{row.feature}</span>
      </Box>
      {!row.separator && (
        <Box
          sx={{
            width: 'full',
            p: 3,
            fontSize: 1,
            textAlign: 'center'
          }}>
          {row[plan].text ? (
            <span>{row[plan].text}</span>
          ) : (
              <span>
                {row[plan].included ? <IncludedIcon sx={{ width: 6 }} /> : <ExcludedIcon sx={{ width: 4 }} />}
              </span>
            )}
        </Box>
      )}
    </Flex>
  )
}