import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Slick } from "./slick.module.css"

export default ({ children, settings = {}, classes = '' }) => {
  const defaults = {
    dots: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    ...settings
  }
  return (
    <Slider
      className={`${Slick} ${classes}`}
      {...defaults}>
      { children }
    </Slider> 
  )
}