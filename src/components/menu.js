/** @jsx jsx */
import { jsx, Container, Flex } from "theme-ui"
import { useMobile } from "../hooks/useMobile"
import Logo from "./logo"
import Navigation from "./navigation"

//color passed from header.js
const Menu = ({ color }) => {
  const [mobile] = useMobile()
  return (
    <Flex
      sx={{
        position: ['fixed', 'initial'],
        width: ['screenWidth', 'auto'],
        height: ['screenHeight', 'auto'],
        top: 0,
        bg: ['white', 'transparent'],
        overflowY: 'scroll',
        transition: `transform 300ms ease-in-out`,
        transform: [mobile ? `none` : `translateX(100%)`, `none`]
      }}>
      <Container
        sx={{
          px: 4
        }}>
        <Flex
          sx={{
            mx: -4,
            alignItems: ['flex-start', 'center'],
            flexDirection: ['column', 'row'],
            py: [3, 2]
          }}>
          <Logo color={mobile ? 'primary' : color} />
          <Navigation color={color} />
        </Flex>
      </Container>
    </Flex>
  )
}

export default Menu