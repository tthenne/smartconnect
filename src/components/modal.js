/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui"
import { RiCloseCircleFill } from "react-icons/ri"

export default ({ children, close }) => {
  return (
    <Flex
      sx={{
        position: 'fixed',
        zIndex: 999999,
        top: 0, right: 0, bottom: 0, left: 0,
        bg: `rgba(255,255,255,0.9)`,
        justifyContent: 'center',
        alignItems: 'center'
      }}>
      <Box
        sx={{
          position: 'relative',
          width: ['full', '3/4', '2/3', '1/2'],
          bg: 'white',
          p: 5,
          boxShadow: '2xl',
          borderRadius: 20
        }}>
        <RiCloseCircleFill
          onClick={ e => {
            e.preventDefault()
            close()
          }}
          size="1.5em"
          sx={{
            position: 'absolute',
            cursor: 'pointer',
            top: 3,
            right: 4
          }} />
        <Flex
          sx={{
            width: 'full',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          { children }
        </Flex>
      </Box>
    </Flex>
  )
}