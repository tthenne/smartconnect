/** @jsx jsx */
import { jsx } from "theme-ui"
import { Sling as Hamburger } from "hamburger-react"
import { useMobile } from "../hooks/useMobile"

const HamburgerMenu = ({ color }) => {
  const [mobile, setMobile] = useMobile()
  return (
    <div
      sx={{
        display: ['block', 'none'],
        position: 'absolute',
        zIndex: 999999,
        top: 3,
        right: 4,
        color: mobile ? 'primary' : color
      }}>
      <Hamburger
        toggled={mobile}
        toggle={setMobile} />
    </div>
  )
}

export default HamburgerMenu