/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"

export default ({ resource: { title = '', description = '', link = ''} }) => {
  return (
    <Flex
      sx={{
        p: 5,
        flexDirection: 'column',
        bg: 'white',
        border: theme => `1px solid ${theme.colors.gray[1]}`,
        borderRadius: 10,
        boxShadow: 'lg'
      }}>
        <Box>
          <Styled.h3>{ title }</Styled.h3>
        </Box>
        <Box>
          <p>{ description }</p>
        </Box>
        <Box>
          <a
            sx={{
              textDecoration: 'underline',
              fontSize: 1,
              fontWeight: 'bold',
              color: 'primary'
            }}
            target="_blank"
            rel="noreferrer"
            href={ link }>view</a>
        </Box>
      </Flex>
  )
}