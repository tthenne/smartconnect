/** @jsx jsx */
import { jsx, Flex, Box, Button, Styled } from "theme-ui"
import { connect } from "react-redux"

import { AiOutlineReload } from "react-icons/ai"

import * as actions from "../../../state/actions/services"
import filterSolution from "../filterSolution"
import SolutionCard from "../solutionCard"

const Services = ({ active, allServicesCount, loadMore }) => {
  return (
    <Flex
      id="services"
      sx={{
        flexDirection: 'column',
        mb: 5
      }}>
      <Box
        sx={{
          mb: 3
        }}>
        <Styled.h2
          sx={{
            color: 'primary',
            fontWeight: 'black'
          }}>Services</Styled.h2>
      </Box>
      <Flex
        sx={{
          flexWrap: 'wrap',
          mx: -2,
          mb: 4
        }}>
        { Boolean(active.length)
          ? ( active.map(service => (
              <Box 
                key={service.databaseId}
                sx={{
                  width: ['full', '1/3'],
                  px: 2,
                  mb: 4
                }}>
                <SolutionCard
                  solution={service} />
              </Box>
            )))
          : (
          <Box
            sx={{
              width: 'full',
              textAlign: 'center',
              color: 'gray.7'
            }}>
              <span>Sorry, no services match your search criteria. Please refine your search.</span>
          </Box>
        )} 
      </Flex>
      <Box>
        <Button
          onClick={e => {
            loadMore(3)
          }}
          disabled={active.length >= allServicesCount || active.length < 6 || !active.length}
          variant="more" 
          sx={{
            width: 'full',
          }}>
            <AiOutlineReload />
            <span
              sx={{
                ml: 3
              }}>Load More</span>
          </Button>
      </Box>
    </Flex>
  )
}

const mapStateToProps = state => {
  const active = filterSolution(state.services.allServices, state.selected, state.services.index)

  return {
    active,
    allServicesCount: state.services.allServices.length
  }
}

const mapActionsToProps = dispatch => ({
  loadMore: (count) => { dispatch({ type: actions.MORE_SERVICES, payload: count }) }
})

export default connect(mapStateToProps, mapActionsToProps)(Services)