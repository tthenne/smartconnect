/** @jsx jsx */
import { Link } from "gatsby"
import { jsx, Flex, Box, Styled } from "theme-ui"

import CloudIcon from "../../images/icons/marketplace/cloud.inline.svg"
import OnpremiseIcon from "../../images/icons/marketplace/onpremise.inline.svg"

const type = {
  WpConnection: 'connection',
  WpTemplate: 'template',
  WpService: 'service',
}
export default ({ solution }) => {
  return (
    <Link
      to={`/${type[solution['__typename']]}/${solution.slug}`}
      sx={{
        display: 'flex',
        height: 'full',
        flexDirection: 'column',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'gray.3',
        borderRadius: 4,
        ':hover': {
          textDecoration: 'none',
          boxShadow: 'md'
        }
      }}>
      <Flex
        sx={{
          flexGrow: 1,
          p: 3,
          flexDirection: 'column'
        }}>
        <Flex
          sx={{
            justifyContent: 'space-between',
            mb: 3
          }}>
          {solution.integrationSolutionDetails.wpcfIcon ? (
            <img
              sx={{
                width: 16
              }}
              src={solution.integrationSolutionDetails.wpcfIcon.localFile.publicURL} alt={solution.title} />
          ) : (
              <Box
                sx={{ width: 16, height: 16, bg: 'gray.4', borderRadius: 10, mx: 2 }}></Box>
            )}
        </Flex>
        <Box>
          <Styled.h5
            sx={{
              color: 'primary',
              fontWeight: 'black'
            }}>{solution.title}</Styled.h5>
        </Box>
        <Box>
          <p
            sx={{
              fontSize: 1
            }}>{solution.integrationSolutionDetails.wpcfShortDescription}</p>
        </Box>
      </Flex>
      <Flex
        sx={{
          p: 3,
          justifyContent: 'space-between',
          alignItems: 'center',
          borderTop: theme => `1px solid ${theme.colors.gray[3]}`
        }}>
        <Flex
          sx={{
            alignItems: 'center'
          }}>
          {
            /** OnPremise check */
            solution.smartConnectProducts.nodes.find(({ termTaxonomyId }) => termTaxonomyId === 16) && (
              <OnpremiseIcon sx={{ mr: 2 }} />
            )
          }
          {
            /** Cloud check */
            solution.smartConnectProducts.nodes.find(({ termTaxonomyId }) => termTaxonomyId === 20) && (
              <CloudIcon sx={{ mr: 2 }} />
            )
          }
        </Flex>
        <Box>
          {
            solution.integrationSolutionDetails.wpcfHighlight !== "none" && (
              <small sx={{
                display: 'inline-block',
                color: 'gray.6',
                fontSize: '0.75rem',
                fontWeight: 'bold',
                borderRadius: 'full'
              }}>{solution.integrationSolutionDetails.wpcfHighlight === "soon" ? "coming soon" : solution.integrationSolutionDetails.wpcfHighlight}</small>
            )
          }
        </Box>
      </Flex>
    </Link>
  )
}