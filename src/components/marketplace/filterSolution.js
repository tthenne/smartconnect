const filterByConnection = (solutions, selected) => {
  if(!selected.length) {
    // selected connection array is empty return all 
    return solutions 
  }

  return solutions.filter(({ smartConnectConnections: { nodes: endpoints }}) => endpoints.find(({termTaxonomyId}) => selected.includes(termTaxonomyId)) )
}

const filterByProducts = (solutions, selected) => {
  if(!selected.length) {
    // selected product array is empty return all
    return solutions 
  }

  return solutions.filter(({ smartConnectProducts: { nodes: endpoints }}) => endpoints.find(({termTaxonomyId}) => selected.includes(termTaxonomyId)) )
}

const filterByCategories = (solutions, selected) => {
  if(!selected.length) {
    // no ScCategories selected return all
    return solutions
  }

  return solutions.filter(({ smartConnectCategories: { nodes: categories}}) => categories.find(({ termTaxonomyId }) => selected.includes(termTaxonomyId)) )
}

const filterByHighlight = (solutions, selected) => {
  if(!selected.length){
    //empty array assume all highlights
    return solutions
  }

  return solutions.filter(({ integrationSolutionDetails: { wpcfHighlight }}) => selected.includes(wpcfHighlight))
}

const filterBySearch = (solutions, search) => {
  if(!search.length){
    //search string empty return all
    return solutions
  }

  return solutions.filter(({ title }) => title.toLowerCase().includes(search))
}

// used to load more connections/templates/services
const slicedSolutions = (solutions, index) => solutions.slice(0, index)

export default (allSolutions, selected, index) => {
  return slicedSolutions(
    //first filter by products
    filterByProducts(
      //second filter the connections
      filterByConnection(
        //third filter by categories
        filterByCategories(
          //forth filter by highlight
          filterByHighlight(
            //last filter by search
            filterBySearch(
              allSolutions,
              selected.search
            ),
            selected.highlights
          ),
          selected.categories
        ),
        selected.connections
      ),
      selected.products
    ),
    index
  )
}