/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { connect } from "react-redux"

import * as selectedActions from "../../../state/actions/selected"

import Checkbox from "./checkbox"

const FilterHighlights = ({ selectedHighlights, addHighlight, removeHighlight }) => {
  const handleSelectedHighlight = ({ target: { value } }) => {
    if (selectedHighlights.includes(value)) {
      //remove
      removeHighlight(value)
    } else {
      //add
      addHighlight(value)
    }
  }
  return (
    <Flex
      as="aside"
      sx={{
        flexDirection: 'column',
        my: 4
      }}>
      <Box>
        <Styled.h6
          sx={{
            fontWeight: 'normal',
            textTransform: 'uppercase',
            color: 'primary',
            mb: 3
          }}>Highlights</Styled.h6>
      </Box>
      <Box
        sx={{
          fontSize: 0,
          mb: 2
        }}>
        <Checkbox
          name="popular"
          change={handleSelectedHighlight}
          checked={selectedHighlights.includes("popular")}
          value="popular">
          <span>Popular</span>
        </Checkbox>
      </Box>
      <Box
        sx={{
          fontSize: 0,
          mb: 2
        }}>
        <Checkbox
          name="new"
          change={handleSelectedHighlight}
          checked={selectedHighlights.includes("new")}
          value="new">
          <span>New</span>
        </Checkbox>
      </Box>
      <Box
        sx={{
          fontSize: 0,
          mb: 2
        }}>
        <Checkbox
          name="soon"
          change={handleSelectedHighlight}
          checked={selectedHighlights.includes("soon")}
          value="soon">
          <span>Coming Soon</span>
        </Checkbox>
      </Box>
    </Flex>
  )
}

const mapStateToProps = state => ({
  selectedHighlights: state.selected.highlights
})

const mapActionsToProps = dispatch => ({
  addHighlight: (name) => { dispatch({ type: selectedActions.ADD_HIGHLIGHT, payload: name }) },
  removeHighlight: (name) => { dispatch({ type: selectedActions.REMOVE_HIGHLIGHT, payload: name }) },
})

export default connect(mapStateToProps, mapActionsToProps)(FilterHighlights)