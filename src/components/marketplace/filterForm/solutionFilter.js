/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { connect } from "react-redux"

import * as selectedActions from "../../../state/actions/selected"

import Checkbox from "./checkbox"

const FilterSolution = ({ selectedSolutions, addSolution, removeSolution }) => {
  const handleSelectedSolution = ({ target: { value } }) => {
    if (selectedSolutions.includes(value)) {
      //remove
      removeSolution(value)
    } else {
      //add
      addSolution(value)
    }
  }
  return (
    <Flex
      as="aside"
      sx={{
        flexDirection: 'column',
        my: 4
      }}>
      <Box>
        <Styled.h6
          sx={{
            fontWeight: 'normal',
            textTransform: 'uppercase',
            color: 'primary',
            mb: 3
          }}>Type</Styled.h6>
      </Box>
      <Box
        sx={{
          fontSize: 0,
          mb: 2
        }}>
        <Checkbox
          name="filter-connections"
          change={handleSelectedSolution}
          checked={selectedSolutions.includes("connections")}
          value="connections">
          <span>Connections</span>
        </Checkbox>
      </Box>
      <Box
        sx={{
          fontSize: 0,
          mb: 2
        }}>
        <Checkbox
          name="filter-templates"
          change={handleSelectedSolution}
          checked={selectedSolutions.includes("templates")}
          value="templates">
          <span>Templates</span>
        </Checkbox>
      </Box>
      <Box
        sx={{
          fontSize: 0,
          mb: 2
        }}>
        <Checkbox
          name="filter-services"
          change={handleSelectedSolution}
          checked={selectedSolutions.includes("services")}
          value="services">
          <span>Services</span>
        </Checkbox>
      </Box>
    </Flex>
  )
}

const mapStateToProps = state => ({
  selectedSolutions: state.selected.solutions
})

const mapActionsToProps = dispatch => ({
  addSolution: (name) => { dispatch({ type: selectedActions.ADD_SOLUTION, payload: name }) },
  removeSolution: (name) => { dispatch({ type: selectedActions.REMOVE_SOLUTION, payload: name }) },
})

export default connect(mapStateToProps, mapActionsToProps)(FilterSolution)