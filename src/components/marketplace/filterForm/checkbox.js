/** @jsx jsx */
import { jsx, Box, Label } from "theme-ui"

const checkboxStyles = {
  mr: 2,
  cursor: "pointer",
  WebkitAppearance: "none",
  MozAppearance: "none",
  appearance: "none",
  outline: 0,
  height: 5,
  width: 5,
  border: "1px solid",
  borderColor: "gray.4",
  borderRadius: 3,
  display: "inline-block",
  ":checked": {
    background: theme => `${theme.colors.lightblue} url('/images/check.svg') center / 10px no-repeat`,
    borderColor: "lightblue",
    position: "relative",
    outline: "none",
  },
  ":focus": {
    boxShadow: "none"
  }
};

const Checkbox = ({ children, name, value, change, checked }) => (
  <Label
    sx={{ cursor: 'pointer' }}
    htmlFor={name}>
    <Box
      as="input"
      type="checkbox"
      sx={checkboxStyles}
      onChange={change}
      defaultChecked={checked}
      name={name}
      id={name}
      value={value} />
      { children } 
  </Label>
)

export default Checkbox