/** @jsx jsx */
import { jsx, Box, Flex } from "theme-ui"
import { connect } from "react-redux"
import { BsSearch } from "react-icons/bs"
import * as selectedActions from "../../../state/actions/selected"

const Search = ({ search, update, clearSearch}) => {
  return (
    <Box
      as="form"
      sx={{
        mb: 4
      }}
      onSubmit={e => e.preventDefault()}>
      <Flex
        sx={{
          position: 'relative'
        }}>
        <input
          type="text"
          name="search"
          placeholder="Find your connection, template or service"
          onChange={e => {
            update(e.target.value.toLocaleLowerCase())
          }}
          value={search}
          sx={{
            flexGrow: 1,
            border: theme => `1px solid ${theme.colors.gray[4]}`,
            borderRadius: `5px 0 0 5px`,
            px: 3,
            py: 2,
            color: 'gray.5',
            '&:focus': {
              outline: 0
            }
          }} />
          <button
            onClick={clearSearch}
            sx={{
              display: ['none', 'block'],
              position: 'absolute',
              top: '13px',
              right: 5,
              border: 0,
              bg: 'transparent',
              color: 'gray.5'
            }}>
            clear
          </button>
          <Box
            sx={{
              display: 'block',
              py: 2,
              px: 3,
              borderStyle: `solid`,
              borderColor: 'gray.4',
              borderWidth: `1px 1px 1px 0`,
              borderRadius: `0 5px 5px 0`,
              color: 'gray.6',
              bg: 'gray.2'
            }}>
            <BsSearch />
          </Box>
      </Flex>
    </Box>
  )
}

const mapStateToProps = state => ({
  search: state.selected.search
})

const mapActionsToProps = dispatch => ({
  update: (value) => { dispatch({ type: selectedActions.SET_SEARCH, payload: value}) },
  clearSearch: () => { dispatch({ type: selectedActions.SET_SEARCH, payload: '' })}
})

export default connect(mapStateToProps, mapActionsToProps)(Search)