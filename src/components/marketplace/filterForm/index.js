/** @jsx jsx */
import { jsx, Box } from "theme-ui"

import SolutionFilter from "./solutionFilter"
import ProductFilter from "./productFilter"
import ConnectionFilter from "./connectionFilter"
import CategoriesFilter from "./categoriesFilter"
import HighlightFilter from "./highlightFilter"

const FilterForm = ({ products, connections, categories }) => {
  return (
    <Box
      as='form'
      onSubmit={e => e.preventDefault()}>
      <SolutionFilter />
      <ProductFilter
        products={products} /> 
      <ConnectionFilter
        connections={connections} />
      <CategoriesFilter
        categories={categories} />
      <HighlightFilter />
    </Box>
  )
}

export default FilterForm