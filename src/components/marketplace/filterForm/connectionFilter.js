/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { connect } from "react-redux"

import * as selectedActions from "../../../state/actions/selected"

import Checkbox from "./checkbox"

const FilterConnections = ({ selectedConnections, addConnection, removeConnection, connections }) => {
  const handleSelectedConnection = ({ target: { value } }) => {
    const id = Number(value)
    if (selectedConnections.includes(id)) {
      //remove
      removeConnection(id)
    } else {
      //add
      addConnection(id)
    }
  }
  return (
    <Flex
      as="aside"
      sx={{
        flexDirection: 'column',
        my: 4
      }}>
      <Box>
        <Styled.h6
          sx={{
            fontWeight: 'normal',
            textTransform: 'uppercase',
            color: 'primary',
            mb: 3
          }}>Connection</Styled.h6>
      </Box>
      {connections.map(({ name, termTaxonomyId }) => (
        <Box
          sx={{
            fontSize: 0,
            mb: 2
          }}
          key={termTaxonomyId}>
          <Checkbox
            name={`connection-${termTaxonomyId}`}
            change={handleSelectedConnection}
            checked={selectedConnections.includes(termTaxonomyId)}
            value={termTaxonomyId}>
            <span>{ name }</span> 
          </Checkbox>
        </Box>
      ))}
    </Flex>
  )
}

const mapStateToProps = state => ({
  selectedConnections: state.selected.connections
})

const mapActionsToProps = dispatch => ({
  addConnection: (id) => { dispatch({ type: selectedActions.ADD_CONNECTION, payload: id }) },
  removeConnection: (id) => { dispatch({ type: selectedActions.REMOVE_CONNECTION, payload: id }) },
})

export default connect(mapStateToProps, mapActionsToProps)(FilterConnections)