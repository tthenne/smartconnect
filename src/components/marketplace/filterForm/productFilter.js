/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { connect } from "react-redux"

import * as selectedActions from "../../../state/actions/selected"

import CloudIcon from "../../../images/icons/marketplace/cloud.inline.svg"
import OnpremiseIcon from "../../../images/icons/marketplace/onpremise.inline.svg"

import Checkbox from "./checkbox"

const FilterProducts = ({ selectedProducts, addProduct, removeProduct, products }) => {
  const handleSelectedProduct = ({ target: { value } }) => {
    const id = Number(value)
    if (selectedProducts.includes(id)) {
      //remove
      removeProduct(id)
    } else {
      //add
      addProduct(id)
    }
  }
  return (
    <Flex
      as="aside"
      sx={{
        flexDirection: 'column',
        my: 3
      }}>
      <Box>
        <Styled.h6
          sx={{
            fontWeight: 'normal',
            textTransform: 'uppercase',
            color: 'primary',
            mb: 3
          }}>Deployment</Styled.h6>
      </Box>
      {products.map(({ name, termTaxonomyId }) => (
        <Box
          sx={{
            fontSize: 0,
            mb: 2
          }}
          key={termTaxonomyId}>
          <Checkbox
            name={`product-${termTaxonomyId}`}
            change={handleSelectedProduct}
            checked={selectedProducts.includes(termTaxonomyId)}
            value={termTaxonomyId}>
            {termTaxonomyId === 16 ? (
              <Flex sx={{ alignItems: 'center' }}>
                <span sx={{ mr: 3 }}>On-Premise | {name}</span>
                <OnpremiseIcon />
              </Flex>
            ) : (
              <Flex sx={{ alignItems: 'center' }}>
                <span sx={{ mr: 3 }}>Cloud | {name}</span>
                <CloudIcon />
              </Flex>
            )}
          </Checkbox>
        </Box>
      ))}
    </Flex>
  )
}

const mapStateToProps = state => ({
  selectedProducts: state.selected.products
})

const mapActionsToProps = dispatch => ({
  addProduct: (id) => { dispatch({ type: selectedActions.ADD_PRODUCT, payload: id }) },
  removeProduct: (id) => { dispatch({ type: selectedActions.REMOVE_PRODUCT, payload: id }) },
})

export default connect(mapStateToProps, mapActionsToProps)(FilterProducts)