/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { connect } from "react-redux"

import * as selectedActions from "../../../state/actions/selected"

import Checkbox from "./checkbox"

const FilterCategories = ({ selectedCategories, addCategory, removeCategory, categories }) => {
  const handleSelectedCategory = ({ target: { value } }) => {
    const id = Number(value)
    if (selectedCategories.includes(id)) {
      //remove
      removeCategory(id)
    } else {
      //add
      addCategory(id)
    }
  }
  return (
    <Flex
      as="aside"
      sx={{
        flexDirection: 'column',
        my: 4
      }}>
      <Box>
        <Styled.h6
          sx={{
            fontWeight: 'normal',
            textTransform: 'uppercase',
            color: 'primary',
            mb: 3
          }}>App Connection</Styled.h6>
      </Box>
      {categories.map(({ name, termTaxonomyId }) => (
        <Box
          sx={{
            fontSize: 0,
            mb: 2
          }}
          key={termTaxonomyId}>
          <Checkbox
            name={`category-${termTaxonomyId}`}
            change={handleSelectedCategory}
            checked={selectedCategories.includes(termTaxonomyId)}
            value={termTaxonomyId}>
            <span>{ name }</span> 
          </Checkbox>
        </Box>
      ))}
    </Flex>
  )
}

const mapStateToProps = state => ({
  selectedCategories: state.selected.categories
})

const mapActionsToProps = dispatch => ({
  addCategory: (id) => { dispatch({ type: selectedActions.ADD_CATEGORY, payload: id }) },
  removeCategory: (id) => { dispatch({ type: selectedActions.REMOVE_CATEGORY, payload: id }) },
})

export default connect(mapStateToProps, mapActionsToProps)(FilterCategories)