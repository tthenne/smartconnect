/** @jsx jsx */
import { jsx, Flex, Box, Button, Styled } from "theme-ui"
import { connect } from "react-redux"

import { AiOutlineReload } from "react-icons/ai"

import * as actions from "../../../state/actions/templates"
import filterSolution from "../filterSolution"
import SolutionCard from "../solutionCard"

const Templates = ({ active, allTemplatesCount, loadMore }) => {
  return (
    <Flex
      id="templates"
      sx={{
        flexDirection: 'column',
        mb: 5
      }}>
      <Box
        sx={{
          mb: 3
        }}>
        <Styled.h2
          sx={{
            color: 'primary',
            fontWeight: 'black'
          }}>Templates</Styled.h2>
      </Box>
      <Flex
        sx={{
          flexWrap: 'wrap',
          mx: -2,
          mb: 4
        }}>
        { Boolean(active.length)
          ? ( active.map(template => (
              <Box 
                key={template.databaseId}
                sx={{
                  width: ['full', '1/3'],
                  px: 2,
                  mb: 4
                }}>
                <SolutionCard
                  solution={template} />
              </Box>
            )))
          : (
          <Box
            sx={{
              width: 'full',
              textAlign: 'center',
              color: 'gray.7'
            }}>
              <span>Sorry, no templates match your search criteria. Please refine your search.</span>
          </Box>
        )} 
      </Flex>
      <Box>
        <Button
          onClick={e => {
            loadMore(3)
          }}
          disabled={active.length >= allTemplatesCount || active.length < 6 || !active.length}
          variant="more" 
          sx={{
            width: 'full'
          }}>
            <AiOutlineReload />
            <span
              sx={{
                ml: 3
              }}>Load More</span>
        </Button>
      </Box>
    </Flex>
  )
}

const mapStateToProps = state => {
  const active = filterSolution(state.templates.allTemplates, state.selected, state.templates.index) 

  return {
    active,
    allTemplatesCount: state.templates.allTemplates.length
  } 
}

const mapActionsToProps = dispatch => ({
  loadMore: (count) => { dispatch({ type: actions.MORE_TEMPLATES, payload: count })}
})

export default connect(mapStateToProps, mapActionsToProps)(Templates)