/** @jsx jsx */
import { jsx, Container, Flex } from "theme-ui"

const MegaMenu = ({ children }) => {
  return (
    <div
      sx={{
        position: 'absolute',
        width: 'full',
        left: 0,
        top: [0, 'initial'],
        mt: [0, 2],
        zIndex: 9999
      }}>
      <Container
        sx={{
          pt: [0, 4],
          px: 4,
          bg: 'white'
        }}>
        <Flex
          sx={{
            flexWrap: 'wrap',
            borderTop: theme => `3px solid ${theme.colors.primary}`,
            py: [5, 4],
            mx: -4,
            a: {
              display: 'inline-block'
            }
          }}>
          { children } 
        </Flex>
      </Container>
    </div>
  )
}

export default MegaMenu