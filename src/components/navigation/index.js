/** @jsx jsx */
import { jsx, Flex, Box, useThemeUI } from "theme-ui"
import ListItem from "./listItem"
import IntegrationSolutions from "./menus/integrationSolutions"
import Resources from "./menus/resources"
import Product from "./menus/product"
import { MenuProvider } from "../../hooks/useMenu"
import { useMobile } from "../../hooks/useMobile"
import { Link } from "gatsby"

//color passed from header.js
const Navigation = ({ color }) => {
  const [mobile] = useMobile()
  const { theme } = useThemeUI()
  return (
    <Flex
      sx={{
        flexDirection: ['column', 'row'],
        px: 4,
        mt: [4, 0],
        ml: [0, 'auto'],
        width: ['full', 'auto'],
        alignItems: ['flex-start', 'center']
      }}
      as="nav">
      <Flex
        sx={{
          flexWrap: 'wrap',
          flexDirection: ['column', 'row'],
          alignItems: ['flex-start', 'center'],
          listStyle: 'none',
          m: 0,
          mr: [0, 5],
          p: 0
        }}
        as="ul">
        <MenuProvider>
          <ListItem
            id={0}
            link="/"
            text="Product"
          >
            <Product />
          </ListItem>
          <ListItem
            id={1}
            link="/"
            text="Integration Solutions">
            <IntegrationSolutions />
          </ListItem>
          <ListItem
            id={2}
            link="/"
            text="Resources">
            <Resources />
          </ListItem>
          <ListItem
            id={3}
            link="/pricing"
            text="Pricing"
          />
          <ListItem
            id={4}
            link="/"
            text="Partners"
          />
        </MenuProvider>
      </Flex>
      <Box
        sx={{
          mt: [4, 0]
        }}>
        <Link
          to={`/`}
          sx={{
            color: mobile ? 'primary' : color,
            display: 'inline-block',
            px: '3em',
            py: 2,
            borderStyle: 'solid',
            borderWidth: 2,
            textDecoration: 'none',
            borderRadius: 'full'
          }}>Contact Us</Link>
        <Flex
          sx={{
            flexDirection: 'column',
            display: ['flex', 'none']
          }}>
          <a
            href="/#"
            sx={{
              ...theme.buttons.secondary,
              my: 3,
              textAlign: 'center'
            }}>
            See a Demo
              </a>
          <a
            href="/#"
            sx={{
              ...theme.buttons.primary,
              textAlign: 'center'
            }}>
            Sign In
              </a>
        </Flex>
      </Box>
    </Flex>
  )
}

export default Navigation