/** @jsx jsx */
import { jsx, Flex } from "theme-ui"
import { Fragment } from "react"
import { connect } from "react-redux"
import { useMenu } from "../../hooks/useMenu"
import { useMobile } from "../../hooks/useMobile"
import { useWindowScroll } from "../../hooks/useWindowScroll"
import { Link } from "gatsby"
import MegaMenu from "./megaMenu"

const ListItem = ({ theme, children, id, text = '', link = ''}) => {
  const [mobile] = useMobile()
  const inverse = useWindowScroll()
  const color = inverse ? 'primary' : theme === 'light' ? 'white' : 'primary'
  const [{ active }, dispatcher] = useMenu()
  const isActive = id === active
  const linkStyles = {
    mx: 3,
    a: {
      display: 'flex',
      justifyContent: 'center',
      position: 'relative',
      overflow: 'hidden',
      py: 2,
      px: 2,
      fontFamily: 'heading',
      fontWeight: 'bold',
      color: 'primary',
      span: {
        position: 'absolute',
        bottom: 0,
        height: '3px',
        transition: `transform 300ms ease-in-out`,
        transform: `scaleX(0)`,
        bg: 'secondary'
      },
    },
    '&.active': {
      a: {
        span: {
          transform: `scaleX(50)`
        }
      }
    },
    '&:hover': {
      a: {
        textDecoration: 'none'
      }
    }
  }
  return (
    <li
      className={isActive && 'active'}
      sx={
        linkStyles
      }>
      {children
        ? (
          <Fragment>
            <a
              href="/#"
              onClick={e => {
                e.preventDefault()
                dispatcher({
                  type: isActive ? 'deactivate' : 'activate',
                  ...(!isActive && {payload: id})
                })
              }}>
              <div
                sx={{
                  color: mobile ? 'primary' : color 
                }}>{ text }</div>
              <span>&nbsp;</span>
            </a>
            {isActive && (
              <Fragment>
                <Flex
                  role="presentation"
                  onClick={e => {
                    dispatcher({
                      type: 'deactivate'
                    })
                  }}
                  sx={{
                    display: ['flex', 'none'],
                    position: 'absolute',
                    zIndex: 99999,
                    top: 3,
                    left: 4,
                    color: 'primary',
                    alignItems: 'center',
                    fontWeight: 'bold',
                    cursor: 'pointer'
                  }}>
                    <div>&lsaquo;</div>
                    <div sx={{ ml: 3, fontSize: 3 }}>{ text }</div>
                </Flex>
                <MegaMenu>
                  { children }
                </MegaMenu>
              </Fragment>
            )}
          </Fragment>
        )
        : (
          <Link
            to={link}>
            <div
              sx={{
                color: mobile ? 'primary' : color 
              }}>{ text }</div>
            <span>&nbsp;</span>
          </Link>
        )
      }
    </li>
  )
}

const mapStateToProps = state => ({
  theme: state.header.theme
})
export default connect(mapStateToProps)(ListItem)