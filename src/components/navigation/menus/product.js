/** @jsx jsx */
import { jsx, Flex, Box, Styled, useThemeUI } from "theme-ui"
import { Fragment } from "react"
import { useMobile } from "../../../hooks/useMobile"
import { useMenu } from "../../../hooks/useMenu"
import { Link, navigate, useStaticQuery, graphql } from "gatsby"
import ProductTourIcon from "../../../images/icons/productTour.inline.svg"

/* eslint-disable no-unused-vars */
export default () => {
  const { theme } = useThemeUI()
  const [menu, dispatcher] = useMenu()
  const [mobile, setMobile] = useMobile()
  const { wpPage: { productTour: { sections }}} = useStaticQuery(graphql`
    query SectionsQuery {
      wpPage(slug: {eq: "product-tour"}) {
        productTour {
          sections {
            slug
            title
          }
        }
      }
    }
  `)
  return (
    <Fragment>
      <Flex
        sx={{
          flexDirection: 'column',
          width: ['full', '1/3'],
          borderRight: theme => `1px solid ${theme.colors.gray[3]}`,
          px: 4,
          mb: [4, 0]
        }}>
        <Box
          sx={{
            mb: 3
          }}>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Overview
          </Styled.h5>
          <p sx={{
            fontSize: 1
          }}>What is SmartConnect for?</p>
        </Box>
        <Flex
          as="ul"
          sx={{
            flexDirection: 'column',
            alignItems: 'flex-start',
            mb: 4,
            p: 0,
            listStyle: 'none',
            li: {
              a: {
                py: 1,
                px: 0,
                '&:hover': {
                  textDecoration: 'underline'
                }
              }
            }
          }}>
          <li><Link to="/">By Industry</Link></li>
          <li><Link to="/">By Business Function</Link></li>
          <li><Link to="/">By Job Role</Link></li>
        </Flex>
        <Box>
          <a
            onClick={e => {
              e.preventDefault()
              setMobile(false)
              dispatcher({
                type:'deactivate'
              })
              navigate(`/overview`)
            }}
            href={`/overview`}
            sx={{
              ...theme.buttons.inverse,
              color: 'primary',
              borderColor: 'primary'
            }}><div sx={{ px: 3 }}>Product Overview</div></a>
        </Box>
      </Flex>
      <Flex
        sx={{
          flexDirection: 'column',
          width: ['full', '1/3'],
          borderRight: theme => `1px solid ${theme.colors.gray[3]}`,
          px: 4
        }}>
        <Box
          sx={{
            mb: 3
          }}>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Product Tour
          </Styled.h5>
          <p sx={{
            fontSize: 1
          }}>Get to know SmartConnect.</p>
        </Box>
        <Flex
          as="ul"
          sx={{
            flexDirection: 'column',
            mx: 0,
            mb: 3,
            p: 0,
            listStyle: 'none'
          }}>
          {
            sections.map(section => (
              <Flex
                sx={{
                  alignItems: 'center',
                  a: {
                    fontWeight: 'normal',
                    lineHeight: 'tight',
                    "&:hover": {
                      textDecoration: 'underline'
                    }
                  }
                }}
                as="li"
                key={section.slug}>
                <ProductTourIcon />
                <a
                  onClick={e => {
                    e.preventDefault()
                    setMobile(false)
                    dispatcher({
                      type:'deactivate'
                    })
                    navigate(`/product-tour/#${section.slug}`)
                  }}
                  href={`/product-tour/#${section.slug}`}>
                  {section.title}
                </a>
              </Flex>
            ))
          }
        </Flex>
        <Box
          sx={{
            mt: 3
          }}>
          <a
            onClick={e => {
              e.preventDefault()
              setMobile(false)
              dispatcher({
                type:'deactivate'
              })
              navigate(`/product-tour`)
            }}
            href={`/product-tour`}
            sx={{
              ...theme.buttons.inverse,
              color: 'primary',
              borderColor: 'primary'
            }}><div sx={{ px: 3 }}>Take the Product Tour</div></a>
        </Box>
      </Flex>
    </Fragment>
  )
}