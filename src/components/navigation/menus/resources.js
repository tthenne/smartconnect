/** @jsx jsx */
import { jsx, Flex, Box, Styled, Button } from "theme-ui"
import { Fragment } from "react"
import { Link, useStaticQuery, graphql } from "gatsby"
import PlayIcon from "../../../images/icons/play.inline.svg"

const CaseStudy = ({ study }) => {
  return (
    <Flex
      sx={{
        flexDirection: 'column',
        alignItems: 'flex-start',
        pb: 3,
        mb: 4,
        borderBottom: theme => `1px solid ${theme.colors.gray[3]}`,
        a: {
          display: 'inline-block',
          fontWeight: 'normal',
          fontSize: 1,
          px: 0
        }
      }} >
      <img
        sx={{
          float: 'left'
        }}
        src={study.caseStudyFields.logo.localFile.publicURL}
        alt={study.title} />
      <p
        sx={{
          mb: 0,
          fontSize: 1
        }}>{study.caseStudyFields.summary}</p>
      <Link to={`/case-study/${study.slug}`}>Learn More &rsaquo;</Link>
    </Flex>
  )
}


export default () => {
  const { allWpKnowledgeBase: { articles }, allWpCaseStudy } = useStaticQuery(graphql`
    query KnowledgeQuery {
      allWpKnowledgeBase(limit: 4, sort: {order: DESC, fields: date}, filter: {status: {eq: "publish"}}) {
        articles: nodes {
          id
          slug
          title
          status
        }
      }
      allWpCaseStudy(limit: 2) {
        nodes {
          databaseId
          slug
          title
          caseStudyFields {
            logo {
              localFile {
                publicURL
              }
            }
            summary
          }
        }
      }
    }
  `)
  return (
    <Fragment>
      <Flex
        sx={{
          flexDirection: 'column',
          width: ['full', '1/4'],
          borderRight: theme => `1px solid ${theme.colors.gray[3]}`,
          px: 4,
          mb: 4
        }}>
        <Box
          sx={{
            mb: 3
          }}>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Watch
          </Styled.h5>
          <p sx={{
            fontSize: 1
          }}>Watch our latest resource videos.</p>
        </Box>
        <Flex
          sx={{
            flexDirection: 'column'
          }}>
          <Box>
            <Styled.h5>
              Introduction to SmartConnect
              </Styled.h5>
          </Box>
          <Box
            sx={{
              mt: 2,
              mb: 4
            }}>
            <div
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                height: '150px',
                bg: 'primary'
              }}>
              <PlayIcon />
            </div>
          </Box>
          <Flex
            sx={{
              fontSize: 0,
            }}>
            <Button
              sx={{
                px: 3,
                mr: 3,
                width: '1/2'
              }}
              variant="inverse">See All Videos
              </Button>
            <Button
              sx={{
                width: '1/2'
              }}
              variant="secondary">Watch a Demo
              </Button>
          </Flex>
        </Flex>
      </Flex>
      <Flex
        sx={{
          flexDirection: 'column',
          width: ['full', '1/4'],
          borderRight: theme => `1px solid ${theme.colors.gray[3]}`,
          px: 4
        }}>
        <Box
          sx={{
            mb: 3
          }}>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Knowledge Base
          </Styled.h5>
          <p sx={{
            fontSize: 1
          }}>Checkout our recent content.</p>
        </Box>
        <Flex
          sx={{
            flexDirection: 'column'
          }}>
          {articles.map(article => (
            <Flex
              key={article.id}
              sx={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                pb: 3,
                mb: 4,
                borderBottom: theme => `1px solid ${theme.colors.gray[3]}`,
                a: {
                  pt: 0,
                  pb: '1px',
                  bg: 'primary',
                  color: 'white'
                }
              }}>
              <Styled.h6
                sx={{
                  color: 'primary',
                  m: 0,
                  mb: 3
                }}>{article.title}</Styled.h6>
              <Link
                sx={{
                  p: 0,
                  fontSize: 1,
                  lineHeight: 1.5,
                  border: theme => `1px solid ${theme.colors.primary}`
                }}
                to={`/knowledge-base/${article.slug}`}>
                <small>Read It</small><small sx={{ ml: 2 }}>&rsaquo;</small>
              </Link>
            </Flex>
          ))}
        </Flex>
        <Box>
          <Link
            sx={{
              fontSize: 0
            }}
            to={`/knowledge-base`}>
            View All Knowledge Base 
              <small sx={{
              fontSize: 0,
              ml: 2
            }}>&rsaquo;</small>
          </Link>
        </Box>
      </Flex>
      <Flex
        sx={{
          flexDirection: 'column',
          width: ['full', '1/4'],
          borderRight: theme => `1px solid ${theme.colors.gray[3]}`,
          px: 4
        }}>
        <Box
          sx={{
            mb: 3
          }}>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Case Studies
          </Styled.h5>
          <p sx={{
            fontSize: 1
          }}>Our latest and greatest case studies.</p>
        </Box>
        <Flex
          sx={{
            flexDirection: 'column'
          }}>
          {allWpCaseStudy.nodes.map(study => (
            <CaseStudy
              key={study.id}
              study={study} />
          ))}
        </Flex>
      </Flex>
      <Flex
        sx={{
          flexDirection: 'column',
          width: ['full', '1/4'],
          px: 4
        }}>
        <Box>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Help & Success Content
          </Styled.h5>
          <p sx={{
            fontSize: 1
          }}>We want to help you with your next project.</p>
        </Box>
        <Flex
          as="ul"
          sx={{
            flexDirection: 'column',
            alignItems: 'flex-start',
            mb: 4,
            p: 0,
            listStyle: 'none',
            li: {
              a: {
                fontSize: 1,
                py: 1,
                px: 0,
                '&:hover': {
                  textDecoration: 'underline'
                }
              }
            }
          }}>
          <li><Link to="/">Documentation & Downloads</Link></li>
          <li><Link to="/">Training & Certification</Link></li>
          <li><Link to="/">Community Forums</Link></li>
          <li><Link to="/">eBooks & Guides</Link></li>
        </Flex>
        <Box>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Support
          </Styled.h5>
          <p sx={{
            fontSize: 1
          }}>We've got you covered.</p>
        </Box>
        <Flex>
          <Box
            sx={{
              display: 'grid',
              width: 'full',
              minHeight: '250px',
              gridGap: '3px',
              gridTemplateRows: '1fr 2fr 2fr',
              gridTemplateColumns: '2fr 2fr 2fr'
            }}>
            <Flex
              sx={{
                p: 2,
                px: 3,
                bg: '#628EBE',
                gridColumn: '1/4',
                justifyContent: 'space-between',
                alignItems: 'center',
                color: 'white',
              }}>
              <Box>
                <Styled.h6 sx={{ m: 0 }}>Submit Support Ticket</Styled.h6>
              </Box>
              <Box>
                <Button
                  sx={{
                    px: 3,
                    py: 1,
                    fontSize: 0,
                    bg: 'transparent',
                    borderStyle: 'solid',
                    borderWidth: '1px',
                    borderColor: 'white',
                    fontWeight: 'normal',
                    color: 'white'
                  }}>Submit</Button>
              </Box>
            </Flex>
            <Flex
              sx={{
                p: 2,
                bg: '#76A1D0',
                gridRow: '2/3',
                gridColumn: '1/3',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: 'white'
              }}>
              <Box
                sx={{
                  textAlign: 'center'
                }}>
                <Styled.h6>Knowledge Base</Styled.h6>
              </Box>
              <Box
                sx={{
                  textAlign: 'center'
                }}>
                <Button
                  sx={{
                    px: 3,
                    py: 1,
                    fontSize: 0,
                    bg: 'transparent',
                    borderStyle: 'solid',
                    borderWidth: '1px',
                    borderColor: 'white',
                    fontWeight: 'normal',
                    color: 'white'
                  }}>All KB Articles</Button>
              </Box>
            </Flex>
            <Flex
              sx={{
                p: 2,
                py: 3,
                bg: '#BAD2F2',
                gridRow: '2/4',
                gridColumn: '3/4',
                flexDirection: 'column',
                justifyContent: 'space-between',
                alignItems: 'center',
                color: 'white'
              }}>
              <Box
                sx={{
                  textAlign: 'center'
                }}>
                <Styled.h6>Email an Expert</Styled.h6>
              </Box>
              <Box
                sx={{
                  textAlign: 'center',
                  mb: '6px'
                }}>
                <Button
                  sx={{
                    px: 2,
                    py: 1,
                    fontSize: '0.65rem',
                    bg: 'transparent',
                    borderStyle: 'solid',
                    borderWidth: '1px',
                    borderColor: 'white',
                    fontWeight: 'normal',
                    color: 'white',
                  }}>Email Us</Button>
              </Box>
            </Flex>
            <Flex
              sx={{
                p: 2,
                bg: '#89ADDE',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: 'white',
                gridColumn: '1/2'
              }}>
              <Box
                sx={{
                  textAlign: 'center'
                }}>
                <Styled.h6>Chat</Styled.h6>
              </Box>
              <Box
                sx={{
                  textAlign: 'center'
                }}>
                <Button
                  sx={{
                    px: 2,
                    py: 1,
                    fontSize: '0.65rem',
                    bg: 'transparent',
                    borderStyle: 'solid',
                    borderWidth: '1px',
                    borderColor: 'white',
                    fontWeight: 'normal',
                    color: 'white'
                  }}>Start Chat</Button>
              </Box>
            </Flex>
            <Flex
              sx={{
                p: 2,
                bg: '#8CB7F2',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: 'white',
                gridColumn: '2/3'
              }}>
              <Box
                sx={{
                  textAlign: 'center'
                }}>
                <Styled.h6>Call</Styled.h6>
              </Box>
              <Box
                sx={{
                  textAlign: 'center'
                }}>
                <Button
                  sx={{
                    px: 2,
                    py: 1,
                    fontSize: '0.65rem',
                    bg: 'transparent',
                    borderStyle: 'solid',
                    borderWidth: '1px',
                    borderColor: 'white',
                    fontWeight: 'normal',
                    color: 'white'
                  }}>Call Us</Button>
              </Box>
            </Flex>
          </Box>
        </Flex>
      </Flex>
    </Fragment>
  )
}