/** @jsx jsx */
import { jsx, Flex, Box, Styled } from "theme-ui"
import { Fragment } from "react"
import TemplateIcon from "../../../images/icons/templateIcon.inline.svg"
import MicrosoftIcon from "../../../images/icons/microsoft.inline.svg"
import SalesforceIcon from "../../../images/icons/salesforce.inline.svg"
import ZendeskIcon from "../../../images/icons/zendesk.inline.svg"
import NetsuiteIcon from "../../../images/icons/netsuite.inline.svg"
import MagentoIcon from "../../../images/icons/magento.inline.svg"
import ShopifyIcon from "../../../images/icons/shopify.inline.svg"
import { Link } from "gatsby"

export default () => {
  const templates = [
    {
      id: 1,
      title: 'Dynamics 365 - Dynamics GP',
      link: '/'
    },
    {
      id: 2,
      title: 'Salesforce - Dynamics GP',
      link: '/'
    },
    {
      id: 3,
      title: 'Dynamics 365 Business Central - Dynamics',
      link: '/'
    },
    {
      id: 4,
      title: 'Dynamics 365 Business Central - Salesforce',
      link: '/'
    },
    {
      id: 5,
      title: 'Dynamics NAV - Salesforce',
      link: '/'
    },
    {
      id: 6,
      title: 'SAP Concur – Dynamics 365 Business Central',
      link: '/'
    },
    {
      id: 7,
      title: 'SAP Concur – Dynamics GP',
      link: '/'
    },
    {
      id: 8,
      title: 'General Journals to Dynamics GP',
      link: '/'
    },
    {
      id: 9,
      title: 'Zendesk – Dynamics 365',
      link: '/'
    },
    {
      id: 10,
      title: 'Zendesk – Salesforce',
      link: '/'
    },
  ]
  const connections = [
    {
      id: 1,
      title: 'Microsoft Dynamics 365',
      link: '/',
      icon: MicrosoftIcon,
      children: [
        {
          id: 1,
          title: 'Dynamics 365 Business Central',
          link: '/',
        },
        {
          id: 2,
          title: 'Dynamics 365 Sales & Power Platform',
          link: '/',
        },
        {
          id: 3,
          title: 'Dynamics 365 Field Service',
          link: '/',
        },
        {
          id: 4,
          title: 'Dynamics 365 Project Service Automation',
          link: '/',
        },
        {
          id: 5,
          title: 'Dynamics 365 Customer Service',
          link: '/',
        },
        {
          id: 6,
          title: 'Dynamics 365 Finance & Operations',
          link: '/',
          comingSoon: true
        },
      ]
    },
    {
      id: 2,
      title: 'Microsoft Dynamics NAV',
      link: '/',
      icon: MicrosoftIcon,
      children: []
    },
    {
      id: 3,
      title: 'Micorsoft Dynamics GP',
      link: '/',
      icon: MicrosoftIcon,
      children: []
    },
    {
      id: 4,
      title: 'Salesforce',
      link: '/',
      icon: SalesforceIcon,
      children: []
    },
    {
      id: 5,
      title: 'Zendesk',
      link: '/',
      icon: ZendeskIcon,
      children: []
    },
    {
      id: 6,
      title: 'Netsuite',
      link: '/',
      icon: NetsuiteIcon,
      children: []
    },
    {
      id: 7,
      title: 'Magento',
      link: '/',
      icon: MagentoIcon,
      children: []
    },
    {
      id: 8,
      title: 'Shopify',
      link: '/',
      icon: ShopifyIcon,
      children: []
    },
  ]
  return (
    <Fragment>
      <Flex
        sx={{
          flexDirection: 'column',
          alignItems: 'flex-start',
          width: ['full', '1/3'],
          px: 4,
        }}>
        <Box>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Connections
          </Styled.h5>
          <p sx={{
            fontSize: 1,
            mb: 0
          }}>Integration endpoints to use a source or destination.</p>
        </Box>
        <Flex
          as="ul"
          sx={{
            flexDirection: 'column',
            mx: 0,
            my: 3,
            p: 0,
            listStyle: 'none'
          }}>
          {
            connections.map(({ id, title, link, icon: Icon, children = [] }) => (
              <Flex
                sx={{
                  flexDirection: 'column',
                  a: {
                    fontWeight: 'normal',
                    "&:hover": {
                      textDecoration: 'underline'
                    }
                  }
                }}
                as="li"
                key={id}>
                <Flex
                  sx={{ alignItems: 'center' }}>
                  <Icon sx={{ width: '25px' }} />
                  <Link
                    sx={{
                    }}
                    to={`${link}`}>
                    {title}
                  </Link>
                </Flex>
                {Boolean(children.length) && (
                  <Flex
                    as="ul"
                    sx={{
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                      mx: 0,
                      mt: 0,
                      mb: 3,
                      pl: 3,
                      listStyle: 'none',
                    }}>
                    {children.map(child => (
                      <li
                        sx={{
                          position: 'relative',
                          lineHeight: 'tight',
                          a: {
                            fontSize: 1,
                            fontWeight: 'normal',
                            py: 1,
                          }
                        }}
                        key={child.id}>
                        <Link
                          sx={{
                          }}
                          to={`${child.link}`}>
                          {child.title}
                        </Link>
                        {(child.hasOwnProperty('comingSoon') && child.comingSoon) && (
                          <div
                            sx={{
                              position: 'absolute',
                              right: '-70px',
                              top: 1,
                              borderRadius: 'full',
                              bg: 'secondary',
                              color: 'white',
                              py: 1,
                              px: 2,
                              ml: 2,
                              fontSize: '0.45rem'
                            }}>Coming Soon</div>
                        )}
                      </li>
                    ))}
                  </Flex>
                )}
              </Flex>
            ))
          }
        </Flex>
        <Box>
          <Link
            sx={{
              fontSize: 0
            }}
            to={`/integration-solutions/#connections`}>
            View All Connections
              <small sx={{
              fontSize: 0,
              ml: 2
            }}>&rsaquo;</small>
          </Link>
        </Box>
      </Flex>
      <Flex
        sx={{
          flexDirection: 'column',
          alignItems: 'flex-start',
          width: ['full', '1/3'],
          px: 4,
          borderStyle: 'solid',
          borderColor: 'gray.3',
          borderWidth: '0 1px'
        }}>
        <Box>
          <Styled.h5
            sx={{
              color: 'primary'
            }}>
            Configuration Templates
          </Styled.h5>
          <p sx={{
            fontSize: 1
          }}>Starting points to accelerate your project.</p>
        </Box>
        <Flex
          as="ul"
          sx={{
            flexDirection: 'column',
            mx: 0,
            mt: 3,
            mb: 4,
            p: 0,
            listStyle: 'none'
          }}>
          {
            templates.map(template => (
              <Flex
                sx={{
                  alignItems: 'center',
                  a: {
                    fontWeight: 'normal',
                    lineHeight: 'tight',
                    "&:hover": {
                      textDecoration: 'underline'
                    }
                  }
                }}
                as="li"
                key={template.id}>
                <TemplateIcon />
                <Link
                  sx={{
                  }}
                  to={`${template.link}`}>
                  {template.title}
                </Link>
              </Flex>
            ))
          }
        </Flex>
        <Box>
          <Link
            sx={{
              fontSize: 0
            }}
            to={`/integration-solutions/#templates`}>
            View All Templates 
              <small sx={{
              fontSize: 0,
              ml: 2
            }}>&rsaquo;</small>
          </Link>
        </Box>
      </Flex>
      <Flex
        sx={{
          flexDirection: 'column',
          alignItems: 'flex-start',
          width: ['full', '1/3'],
          justifyContent: 'space-between',
          px: 4
        }}>
        <Flex
          sx={{
            flexDirection: 'column',
            alignItems: 'flex-start',
            mb: 4
          }}>
          <Box>
            <Styled.h5
              sx={{
                color: 'primary'
              }}>
              Services Offerings
            </Styled.h5>
            <p sx={{
              fontSize: 1
            }}>Templates that eOne can implement for you.</p>
          </Box>
          <Flex
            as="ul"
            sx={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              mx: 0,
              mt: 0,
              mb: 3,
              p: 0,
              listStyle: 'none'
            }}>
            <li
              sx={{
                a: {
                  fontWeight: 'normal',
                  lineHeight: 'tight',
                  "&:hover": {
                    textDecoration: 'underline'
                  }
                }
              }}>
              <Link to="/">Service Offering One</Link>
            </li>
            <li
              sx={{
                a: {
                  fontWeight: 'normal',
                  lineHeight: 'tight',
                  "&:hover": {
                    textDecoration: 'underline'
                  }
                }
              }}>
              <Link to="/">Service Offering Two</Link>
            </li>
            <li
              sx={{
                a: {
                  fontWeight: 'normal',
                  lineHeight: 'tight',
                  "&:hover": {
                    textDecoration: 'underline'
                  }
                }
              }}>
              <Link to="/">Service Offering Three</Link>
            </li>
          </Flex>
          <Box>
          <Link
            sx={{
              fontSize: 0
            }}
            to={`/integration-solutions/#services`}>
            View All Services 
              <small sx={{
              fontSize: 0,
              ml: 2
            }}>&rsaquo;</small>
          </Link>
        </Box>
        </Flex>
        <Flex
          sx={{
            flexDirection: 'column',
          }}>
          <Box>
            <Styled.h5
              sx={{
                color: 'primary'
              }}>
              Custom Integration Services
            </Styled.h5>
            <p sx={{
              fontSize: 1
            }}>Partner with eOne to deliver your specific project.</p>
          </Box>
          <Flex
            as="ul"
            sx={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              mx: 0,
              mt: 0,
              mb: 3,
              p: 0,
              listStyle: 'none'
            }}>
            <li
              sx={{
                a: {
                  fontWeight: 'normal',
                  lineHeight: 'tight',
                  "&:hover": {
                    textDecoration: 'underline'
                  }
                }
              }}>
              <Link to="/">Custom Integration One</Link>
            </li>
            <li
              sx={{
                a: {
                  fontWeight: 'normal',
                  lineHeight: 'tight',
                  "&:hover": {
                    textDecoration: 'underline'
                  }
                }
              }}>
              <Link to="/">Custom Integration Two</Link>
            </li>
            <li
              sx={{
                a: {
                  fontWeight: 'normal',
                  lineHeight: 'tight',
                  "&:hover": {
                    textDecoration: 'underline'
                  }
                }
              }}>
              <Link to="/">Custom Integration Three</Link>
            </li>
          </Flex>
        </Flex>
      </Flex>
    </Fragment>
  )
}