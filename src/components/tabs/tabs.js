/** @jsx jsx */
import { jsx, Flex, Box } from "theme-ui"
import { useTabs } from "../../hooks/useTabs"
const Tabs = ({ tabs }) => {
  const [state, dispatcher] = useTabs()
  const headers = tabs.map(tab => tab.title)
  const contents = tabs.map(tab => tab.content)
  return (
    <Flex
      sx={{
        flexDirection: 'column'
      }}>
      {headers.length > 1 && (
        <Flex
          as="ul"
          sx={{
            p: 0,
            m: 0,
            my: 3,
            listStyle: 'none'
          }}>
          {headers.map((header, i) => (
            <li
              key={i}
              sx={{
                mx: 3
              }}>
              <a
                href="/#"
                sx={{
                  px: 2,
                  py: 2,
                  fontWeight: 'bold',
                  borderStyle: 'solid',
                  borderWidth: `0 0 3px`,
                  borderColor: state.active === i ? 'secondary' : 'transparent',
                  color: state.active === i ? 'primary' : 'gray.6',
                  "&:hover": {
                    textDecoration: 'none'
                  }
                }}
                onClick={e => {
                  e.preventDefault()
                  dispatcher({
                    type: 'activate',
                    payload: i
                  })
                }}>{header}</a>
            </li>
          ))}
        </Flex>
      )}
      <Box
        sx={{
          py: 3,
          p: {
            mt: 0
          }
        }}>
        <div
          dangerouslySetInnerHTML={{
            __html: contents[state.active]
          }} />
      </Box>
    </Flex>
  )
}

export default Tabs