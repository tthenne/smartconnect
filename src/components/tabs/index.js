import React from "react"
import { TabProvider } from "../../hooks/useTabs"
import Tabs from "./tabs"

const TabsWrapper = ({ tabs = [] }) => (
  <TabProvider>
    <Tabs
      tabs={tabs} />
  </TabProvider>
)

export default TabsWrapper