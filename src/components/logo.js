/** @jsx jsx */
import { jsx, Flex } from "theme-ui"
import { Link } from "gatsby"
import EoneIcon from "../images/icons/eoneIcon.inline.svg"
import SmartConnectLogo from "../images/sc.inline.svg"

const Logo = ({ color = 'primary' }) => (
  <Flex
    sx={{
      alignItems: 'center',
      px: 4,
      py: 3,
      width: ['full', 'auto']
    }}>
    <EoneIcon
      sx={{
        width: '35px',
      }} />
    <Link
      sx={{
        lineHeight: 1
      }}
      to="/">
      <SmartConnectLogo
        sx={{
          width: '200px',
          color: color, 
          ml: 3
        }} />
    </Link>
  </Flex>
)

export default Logo