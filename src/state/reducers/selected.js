import * as actions from "../actions/selected"

const initialState = {
  search: '',
  solutions: [],
  connections: [],
  categories: [],
  products: [],
  highlights: []
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.ADD_SOLUTION:
      return {
        ...state,
        solutions: [...state.solutions, action.payload]
      }
    case actions.REMOVE_SOLUTION:
      return {
        ...state,
        solutions: state.solutions.filter(s => s !== action.payload)
      }
    case actions.ADD_CONNECTION:
      return {
        ...state,
        connections: [...state.connections, action.payload]
      }
    case actions.REMOVE_CONNECTION:
      return {
        ...state,
        connections: state.connections.filter(c => c !== action.payload)
      }
    case actions.ADD_CATEGORY:
      return {
        ...state,
        categories: [...state.categories, action.payload]
      }
    case actions.REMOVE_CATEGORY:
      return {
        ...state,
        categories: state.categories.filter(c => c !== action.payload)
      }
    case actions.ADD_PRODUCT:
      return {
        ...state,
        products: [...state.products, action.payload]
      }
    case actions.REMOVE_PRODUCT:
      return {
        ...state,
        products: state.products.filter(h => h !== action.payload)
      }
    case actions.ADD_HIGHLIGHT:
      return {
        ...state,
        highlights: [...state.highlights, action.payload]
      }
    case actions.REMOVE_HIGHLIGHT:
      return {
        ...state,
        highlights: state.highlights.filter(h => h !== action.payload)
      }
    case actions.SET_SEARCH:
      return {
        ...state,
        search: action.payload
      }
    default:
      return state
  }
}

export default reducer