import * as actions from "../actions/templates"

const initalState = {
  index: 6,
  allTemplates: []
}

const reducer = (state = initalState, action) => {
  switch(action.type){
    case actions.ALL_TEMPLATES:
      return {
        ...state,
        allTemplates: [...action.payload]
      } 
    case actions.MORE_TEMPLATES: {
      return {
        ...state,
        index: state.index + action.payload
      } 
    }
    default:
      return state
  }
}

export default reducer