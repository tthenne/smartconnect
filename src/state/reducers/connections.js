import * as actions from "../actions/connections"

const initalState = {
  index: 6,
  allConnections: []
}

const reducer = (state = initalState, action) => {
  switch(action.type){
    case actions.ALL_CONNECTIONS:
      return {
        ...state,
        allConnections: [...action.payload]
      } 
    case actions.MORE_CONNECTIONS: {
      return {
        ...state,
        index: state.index + action.payload
      } 
    }
    default:
      return state
  }
}

export default reducer