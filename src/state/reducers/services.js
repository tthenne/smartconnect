import * as actions from "../actions/services"

const initalState = {
  index: 6,
  allServices: []
}

const reducer = (state = initalState, action) => {
  switch(action.type){
    case actions.ALL_SERVICES:
      return {
        ...state,
        allServices: [...action.payload]
      } 
    case actions.MORE_SERVICES: {
      return {
        ...state,
        index: state.index + action.payload
      } 
    }
    default:
      return state
  }
}

export default reducer