import { combineReducers } from "redux"
import connections from "./connections"
import templates from "./templates"
import services from "./services"
import selected from "./selected"
import knowledge from "./knowledge"
import header from "./header"

export default combineReducers({ connections, templates, services, selected, knowledge, header })