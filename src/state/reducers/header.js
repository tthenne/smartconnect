import * as actions from "../actions/header"

const initalState = {
  theme: "default"
}

const reducer = (state = initalState, action) => {
  switch(action.type){
    case actions.CHANGE_THEME:
      return {
        ...state,
        theme: action.payload
      } 
    default:
      return state
  }
}

export default reducer