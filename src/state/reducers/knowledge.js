import * as actions from "../actions/knowledge"

const initalState = {
  allKnowledgeBase: [],
  search: ''
}

const reducer = (state = initalState, action) => {
  switch(action.type){
    case actions.ALL_KNOWLEDGE_BASE:
      return {
        ...state,
        allKnowledgeBase: [...action.payload]
      } 
    case actions.SET_SEARCH:
      return {
        ...state,
        search: action.payload
      }
    default:
      return state
  }
}

export default reducer