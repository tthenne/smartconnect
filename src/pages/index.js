/** @jsx jsx */
import { jsx, Flex, Box, Container, Styled, Button, useThemeUI } from "theme-ui"
import { useEffect } from "react"
import { Link, graphql } from "gatsby"
import Img from "gatsby-image"
import { connect } from "react-redux"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Animate from "../components/animate"
import { CHANGE_THEME } from "~/state/actions/header"

import Slider from "../components/slider"
import { Hero, Companies } from "../components/slick.module.css"

import Tile from "../components/tile"
import PricingTable from "../components/home/pricingTable"

import ProductTourCard from "../components/home/productTourCard"
import Testimonial from "../components/home/testimonial"
import ConnectionForm from "../components/home/connectionForm"
import DownloadCard from "../components/home/downloadCard"

import ShopifyIcon from "../images/icons/hero/shopify.inline.svg"
import NetsuiteIcon from "../images/icons/hero/netsuite.inline.svg"
import MicrosoftIcon from "../images/icons/hero/microsoft.inline.svg"
import ZendeskIcon from "../images/icons/hero/zendesk.inline.svg"
import QuickbooksIcon from "../images/icons/hero/quickbooks.inline.svg"
import SalesforceIcon from "../images/icons/hero/salesforce.inline.svg"
import StarsIcons from "../images/home/stars.inline.svg"
import DotsIcon from "../images/icons/dots.inline.svg"

const IndexPage = ({ dispatch, data: { allFile, home, tour, connections, downloads } }) => {
  //set the header theme in redux
  useEffect(() => {
    dispatch({ type: CHANGE_THEME, payload: 'light' })
  }, [dispatch])

  const { theme } = useThemeUI()
  return (
    <Layout>
      <SEO title="Home" />
      { /** Hero One **/}
      <section
        sx={{
          backgroundImage: `url('${allFile.nodes.find(({ name }) => name === "home-integration-software-bg").publicURL}')`,
          backgroundSize: `cover`,
          backgroundPosition: `center`,
          pt: '12em',
          pb: 5,
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              alignItems: 'center'
            }}>
            <Flex
              sx={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: ['full', '1/2'],
                px: 4,
                mb: [5, 0]
              }}>
              <Styled.h6
                sx={{
                  color: 'white',
                  textTransform: 'uppercase',
                  fontWeight: 'normal'
                }}>Deliver complex integration without code.</Styled.h6>
              <Styled.h1 sx={{
                color: 'white',
                fontSize: [6, 7],
                mt: 3,
                mb: 5
              }}>
                Integration Software for Mid-Market Companies
              </Styled.h1>
              <Box>
                <Button
                  variant="rounded"
                  sx={{
                    color: 'white'
                  }}>See the Overview Video</Button>
              </Box>
            </Flex>
            <Box
              sx={{
                backgroundImage: `url('${allFile.nodes.find(({ name }) => name === "hero-slider-bg").publicURL}')`,
                backgroundPosition: `center`,
                backgroundRepeat: `repeat-x`,
                backgroundClip: `content-box`,
                width: ['full', '1/2'],
                px: 4,
                textAlign: 'center'
              }}>
              <Slider
                settings={{
                  centerMode: true,
                  centerPadding: '20px',
                  slidesToShow: 5,
                  initialSlide: 2,
                }}
                classes={Hero}>
                <div
                  className="slide">
                  <ShopifyIcon />
                </div>
                <div
                  className="slide">
                  <ZendeskIcon />
                </div>
                <div
                  className="slide">
                  <MicrosoftIcon />
                </div>
                <div
                  className="slide">
                  <NetsuiteIcon />
                </div>
                <div
                  className="slide">
                  <QuickbooksIcon />
                </div>
                <div
                  className="slide">
                  <SalesforceIcon />
                </div>
              </Slider>
            </Box>
          </Flex>
        </Container>
      </section>
      { /** Hero Two **/}
      <section
        sx={{
          px: 4
        }}>
        <Flex
          sx={{
            mx: -4,
            flexWrap: 'wrap',
          }}>
          <Flex
            sx={{
              flexDirection: 'column',
              width: ['full', '5/12'],
              pr: 4,
              pl: [4, `5vw`, `7.5vw`, `12vw`],
              py: 5,
              backgroundImage: `url('${allFile.nodes.find(({ name }) => name === "connected-software-bg").publicURL}')`,
              backgroundSize: `cover`,
              backgroundPosition: `center`,
            }}>
            <Styled.h6
              sx={{
                color: 'white',
                textTransform: 'uppercase',
                fontWeight: 'normal'
              }}>Data integrated your way.</Styled.h6>
            <Styled.h1 sx={{
              color: 'white',
              mt: 3,
              mb: [5, 'auto']
            }}>
              Connected Software = Better Business
              </Styled.h1>
            <Box>
              <Button
                variant="rounded"
                sx={{
                  color: 'white'
                }}>Start a Trial</Button>
            </Box>
          </Flex>
          <Flex
            sx={{
              flexWrap: 'wrap',
              width: ['full', '7/12'],
              pl: 4,
              pr: [4, `5vw`, `7.5vw`, `10vw`],
              py: 5,
              backgroundImage: `url('${allFile.nodes.find(({ name }) => name === "top-rated-bg").publicURL}')`,
              backgroundSize: `cover`,
              backgroundPosition: `center`,
            }}>
            <Flex
              sx={{
                flexDirection: 'column',
                width: ['full', '7/12'],
                pl: [0, 3],
                mb: [5, 0]
              }}>
              <Styled.h6
                sx={{
                  color: 'white',
                  textTransform: 'uppercase',
                  fontWeight: 'normal'
                }}>Configurable, flexible &amp; scalable.</Styled.h6>
              <Styled.h1 sx={{
                color: 'white',
                mt: 3,
                mb: [5, 'auto']
              }}>
                Top-rated iPaaS &amp; Data Integration Software
              </Styled.h1>
              <Box>
                <Button
                  variant="rounded"
                  sx={{
                    color: 'white'
                  }}>Explore Reviews</Button>
              </Box>
            </Flex>
            <Flex
              sx={{
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: ['flex-start', 'flex-end'],
                width: ['full', '5/12'],
                pl: [0, 4]
              }}>
              <Img
                sx={{
                  alignSelf: 'normal',
                  maxWidth: ['xs']
                }}
                fluid={allFile.nodes.find(({ name }) => name === "microsoft-logo-2x").childImageSharp.fluid} alt="Microsoft Logo" />
              <p
                sx={{
                  color: 'white',
                  fontSize: 2
                }}>AppSource</p>
              <StarsIcons />
              <p
                sx={{
                  color: 'white',
                  textAlign: ['left', 'right'],
                  fontSize: 0,
                  lineHeight: 'normal'
                }}>SmartConnect is by far the best integration solution out there. I used to be loyal to another integration product, but after using SmartConnect, I was converted.</p>
            </Flex>
          </Flex>
        </Flex>
      </section>
      { /** Companies Carousel **/}
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4,
            mb: 3
          }}>
          <Flex
            sx={{
              mx: -4,
              justifyContent: "flex-end"
            }}>
            <Box
              sx={{
                px: 4
              }}>
              <Styled.h6
                sx={{
                  color: 'lightblue',
                  textTransform: 'uppercase'
                }}>Powering Integration for 6,000+ Companies</Styled.h6>
            </Box>
          </Flex>
        </Container>
        <Slider
          classes={Companies}
          settings={{
            dots: false,
            centerMode: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplaySpeed: 0,
            speed: 9000,
            cssEase: 'linear',
            draggable: false,
            swipe: false,
            touchMove: false,
            pauseOnFocus: false,
            pauseOnHover: false,
            responsive: [{
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              }
            }]
          }}>
          {home.homepage.companies.map(({ logo }) => (
            <Box
              key={logo.databaseId}>
              <Img
                fixed={logo.localFile.childImageSharp.fixed} alt={logo.localFile.name}
                sx={{
                  filter: `grayscale(1)`,
                  transition: `filter 300ms ease`,
                  '&:hover': {
                    filter: `grayscale(0)`,
                  }
                }} />
            </Box>
          ))}
        </Slider>
      </section>
      { /** Future-proof Data Integration **/}
      <section
        sx={{
          py: 4
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Flex
              sx={{
                flexDirection: 'column',
                px: 4,
                width: ['full', '7/12']
              }}>
              <Styled.h1
                sx={{
                  fontSize: 7,
                  color: 'primary'
                }}>Future-proof your data integration strategy.</Styled.h1>
              <p>Handle your integrations needs now and when things change.  Run your integrations in SmartConnect’s iPAAS or on-premise software, and deliver faster business outcomes.</p>
            </Flex>
            <Flex
              sx={{
                px: 4,
                width: ['full', '5/12'],
                justifyContent: 'flex-end'
              }}>
              <Flex
                as="ul"
                sx={{
                  flexDirection: 'column',
                  m: 0,
                  p: 0,
                  listStyle: 'none',
                  fontWeight: 'bold',
                  color: 'primary'
                }}>
                <Animate
                  speed="animate__faster">
                  <Flex
                    as="li"
                    sx={{
                      alignItems: 'center',
                      mb: 4
                    }}>
                    <DotsIcon />
                    <span sx={{ ml: 3 }}>Enterprise IPAAS Functionality</span>
                  </Flex>
                </Animate>
                <Animate
                  speed="animate__faster"
                  animationDelay="1s">
                  <Flex
                    as="li"
                    sx={{
                      alignItems: 'center',
                      mb: 4
                    }}>
                    <DotsIcon />
                    <span sx={{ ml: 3 }}>100% Configurable Hybrid Integration</span>
                  </Flex>
                </Animate>
                <Animate
                  speed="animate__faster"
                  animationDelay="2s">
                  <Flex
                    as="li"
                    sx={{
                      alignItems: 'center',
                      mb: 4
                    }}>
                    <DotsIcon />
                    <span sx={{ ml: 3 }}>Mid-Market Pricing</span>
                  </Flex>
                </Animate>
                <Animate
                  speed="animate__faster"
                  animationDelay="3s">
                  <Flex
                    as="li"
                    sx={{
                      alignItems: 'center',
                    }}>
                    <DotsIcon />
                    <span sx={{ ml: 3 }}>Above &amp; Beyond Customer Support</span>
                  </Flex>
                </Animate>
              </Flex>
            </Flex>
          </Flex>
        </Container>
      </section>
      { /** Product Tour Homepage */}
      <section
        sx={{
          py: 6
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              mb: 5
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full']
              }}>
              <Styled.h6
                sx={{
                  textTransform: 'uppercase',
                  color: 'lightblue'
                }}>Configure &amp; Manage Integrations with Ease</Styled.h6>
              <Styled.h1
                sx={{
                  color: 'primary',
                  fontSize: 7
                }}>Say, “Yes” to your specific requirement.</Styled.h1>
            </Box>
          </Flex>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            {tour.productTour.sections.map(section => (
              <ProductTourCard
                key={section.slug}
                section={section} />
            ))}
          </Flex>
        </Container>
      </section>
      { /** Testimonial Section */}
      <section
        sx={{
          py: 4
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '3/4'],
                mx: 'auto'
              }}>
              <Animate
                animation="animate__zoomIn"
                threshold={0.1}
                triggerOnce={true}>
                <Testimonial {...home.homepage.testimonial.testimonialACF} />
              </Animate>
            </Box>
          </Flex>
        </Container>
      </section>
      { /** Connection Section */}
      <section
        sx={{
          py: 6
        }}>
        <Flex
          sx={{
            flexWrap: 'wrap'
          }}>
          <Flex
            sx={{
              flexDirection: 'column',
              pr: 4,
              pl: [4, `5vw`, `7.5vw`, `12vw`],
              py: 5,
              width: ['full', '5/12']
            }}>
            <Styled.h6
              sx={{
                textTransform: 'uppercase',
                color: 'lightblue'
              }}>Save time &amp; deliver profitably.</Styled.h6>
            <Styled.h1
              sx={{
                color: 'primary',
                fontSize: 7
              }}>Connect anything, <br />to anything.</Styled.h1>
            <Box
              sx={{
                mt: 5
              }}>
              <Link
                to="/integration-solutions"
                sx={{
                  ...theme.buttons.rounded,
                  bg: 'lightblue',
                  color: 'white',
                  '&:hover': {
                    textDecoration: 'none'
                  }
                }}>View all Connections</Link>
            </Box>
          </Flex>
          <Flex
            sx={{
              flexWrap: 'wrap',
              backgroundImage: `url('/images/connections-bg.png')`,
              backgroundPosition: `right center`,
              backgroundRepeat: `no-repeat`,
              px: 4,
              pr: 5,
              py: 5,
              width: ['full', '7/12'],
              alignItems: 'center',
              justifyContent: 'flex-end'
            }}>
            <Box
              sx={{
                mx: 3,
                ml: '45%',
                alignSelf: 'flex-end',
                mb: 4
              }}>
              <span sx={{
                color: 'gray.5',
                fontSize: 0,
                fontWeight: 'bold',
                textTransform: 'uppercase'
              }}>
                Most Popular
                </span>
            </Box>
            {connections.nodes.map((connection, i) => (
              <Animate
                key={connection.databaseId}
                delay={1000}
                animation="animate__flipInX">
                <Box
                  sx={{
                    width: 24,
                    mx: 3,
                    mb: 3,
                  }}>
                  <img
                    sx={{
                      width: 'full'
                    }}
                    src={connection.integrationSolutionDetails.wpcfIcon.localFile.publicURL}
                    alt={connection.title} />
                </Box>
              </Animate>
            ))}
          </Flex>
        </Flex>
        <Container
          sx={{
            mt: -4,
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              justifyContent: 'center'
            }}>
            <ConnectionForm />
          </Flex>
        </Container>
      </section>
      { /** Support that Knocks */}
      <section
        sx={{
          pt: 5,
          pb: 6,
          mb: -6,
          bg: 'gray.2'
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              mb: 4,
              flexWrap: 'wrap',
              justifyContent: 'center',
              textAlign: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full',]
              }}>
              <Styled.h6
                sx={{
                  color: 'lightblue',
                  textTransform: 'uppercase'
                }}>Integrations can be tricky, getting support shouldn’t be.</Styled.h6>
              <Styled.h1
                sx={{
                  fontSize: 7,
                  color: 'primary',
                  mb: 4
                }}>Support that knocks your socks off</Styled.h1>
              <Button
                variant="inverse">Learn More</Button>
            </Box>
          </Flex>
        </Container>
      </section>
      <section>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              justifyContent: 'center',
              textAlign: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '3/4']
              }}>
              <Flex
                sx={{
                  flexWrap: 'wrap',
                  alignItems: 'center',
                  justifyContent: 'center',
                  bg: 'primary',
                  borderRadius: 25,
                  p: 4,
                  color: 'white'
                }}>
                <Box
                  sx={{
                    p: 3,
                    width: ['full', '1/2', '1/4']
                  }}>
                  <Styled.h1
                    sx={{
                      color: 'white'
                    }}>#1</Styled.h1>
                  <p>Unlimited World<br />Class Support</p>
                </Box>
                <Box
                  sx={{
                    p: 3,
                    width: ['full', '1/2', '1/4']
                  }}>
                  <Styled.h1
                    sx={{
                      color: 'white'
                    }}>6,000+</Styled.h1>
                  <p>Happy<br />Customers</p>
                </Box>
                <Box
                  sx={{
                    p: 3,
                    width: ['full', '1/2', '1/4']
                  }}>
                  <Styled.h1
                    sx={{
                      color: 'white'
                    }}>47</Styled.h1>
                  <p>Countries<br />Worldwide</p>
                </Box>
                <Box
                  sx={{
                    p: 3,
                    width: ['full', '1/2', '1/4']
                  }}>
                  <Styled.h1
                    sx={{
                      color: 'white'
                    }}>280</Styled.h1>
                  <p>Delivery<br />Partners</p>
                </Box>
              </Flex>
            </Box>
          </Flex>
        </Container>
      </section>
      {/** Tiles Section */}
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Flex
              sx={{
                px: 4,
                width: ['full', '1/2'],
                flexDirection: 'column',
                justifyContent: 'space-between'
              }}>
              <Animate
                triggerOnce={true}
                animation="animate__fadeInLeft">
                <Tile
                  title="Templates"
                  subtitle="SmartConnect"
                  icon="/images/templates-icon-2x.png"
                  cta="Grab Yours"
                  orientation={0}>
                  <p>Get a jump start with ready-to-go templates</p>
                </Tile>
              </Animate>
              <Animate
                triggerOnce={true}
                animation="animate__fadeInUp">
                <Tile
                  title="Resources"
                  subtitle="SmartConnect"
                  icon="/images/resources-tile-icon-2x.png"
                  cta="Go to Articles"
                  orientation={0}>
                  <p>Discover more with Knowledge Base Articles</p>
                </Tile>
              </Animate>
            </Flex>
            <Box
              sx={{
                px: 4,
                width: ['full', '1/2']
              }}>
              <Animate
                triggerOnce={true}
                animation="animate__fadeInRight">
                <Tile
                  title="Integration Solutions"
                  subtitle="SmartConnect"
                  icon="/images/integration-solutions-image-2x.png"
                  cta="See Solutions"
                  orientation={1}>
                  <p>Integrate anything, with anything and say, “YES” to your requirements. Whether you’re looking to integrate, migrate…</p>
                </Tile>
              </Animate>
            </Box>
          </Flex>
        </Container>
      </section>
      {/** Downloads */}
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4,
            mb: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Box
              sx={{
                px: 4,
              }}>
              <Styled.h1
                sx={{
                  color: 'primary',
                  fontSize: 7
                }}>Discover More</Styled.h1>
            </Box>
          </Flex>
        </Container>
        <Flex
          sx={{
            flexWrap: 'wrap'
          }}>
          {downloads.nodes.map((download, i, downloads) => (
            <Box
              key={download.databaseId}
              sx={{
                width: ['full', '1/2', '1/4']
              }}>
              <DownloadCard
                last={i === downloads.length - 1}
                download={download} />
            </Box>
          ))}
        </Flex>
      </section>
      {/** Pricing Tables */}
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              flexWrap: 'wrap'
            }}>
            <Flex
              sx={{
                px: 4,
                width: ['full', '1/3'],
                flexDirection: 'column',
                justifyContent: 'center'
              }}>
              <Styled.h6
                sx={{
                  color: 'lightblue',
                  textTransform: 'uppercase',
                  fontSize: 0
                }}>Flexible &amp; Predictable</Styled.h6>
              <Styled.h1
                sx={{
                  fontSize: 7,
                  color: 'primary'
                }}>Pricing</Styled.h1>
              <p>Plans & Payment Options that Fit your Project Needs</p>
              <Box>
                <Button
                  variant="blue">See Pricing Plans</Button>
              </Box>
            </Flex>
            <Flex
              sx={{
                px: 4,
                width: ['full', '2/3'],
                flexDirection: 'column'
              }}>
              {home.homepage.pricing.map((table, i) => (
                <Animate
                  key={i}
                  triggerOnce={true}
                  animation="animate__pulse"
                  threshold={0.75}>
                  <PricingTable
                    table={table} />
                </Animate>
              ))}
            </Flex>
          </Flex>
        </Container>
      </section>
      {/** Learn More SmartConnect */}
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              mx: -4,
              px: 4,
              py: 5,
              bg: 'primary',
              flexWrap: 'wrap',
              alignItems: 'center',
              borderRadius: 10
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '5/12']
              }}>
              <Styled.h1
                sx={{
                  color: 'white',
                  lineHeight: 'tight'
                }}>Learn More about SmartConnect</Styled.h1>
            </Box>
            <Flex
              sx={{
                px: 4,
                width: ['full', '7/12'],
                alignItems: 'center',
                justifyContent: 'center'
              }}>
              <Button
                variant="inverse">
                See a Demo
               </Button>
              <Button
                sx={{
                  mx: 4,
                  fontSize: 2
                }}
                variant="blue">
                Product Tour
               </Button>
              <Button
                variant="inverse">
                30 Day Trial
               </Button>
            </Flex>
          </Flex>
        </Container>
      </section>
      <section
        sx={{
          py: 5
        }}>
        <Container
          sx={{
            px: 4
          }}>
          <Flex
            sx={{
              flexWrap: 'wrap',
              mx: 'auto',
              width: ['full', '3/4'],
              alignItems: 'center'
            }}>
            <Box
              sx={{
                px: 4,
                width: ['full', '1/3']
              }}>
              <Styled.h6
                sx={{
                  textTransform: 'uppercase'
                }}>Industrial Air Centers</Styled.h6>
              <p><em>“In less than 30 mintues I had an Excel integration map  up and running and was onto my next project. About 99.9% of the maps I completed myself. For the remaining .01% I was able to reference eOne’s Support team.”</em></p>
            </Box>
            <Box
              sx={{
                px: 4,
                width: ['full', '7/12'],
                ml: [0, 'auto']
              }}>
              <Styled.h2
                sx={{
                  color: 'primary'
                }}>SmartConnect Toolset makes data integration easy. Drive value faster.</Styled.h2>
              <p>
                <small
                  sx={{
                    fontSize: 0,
                    fontWeight: 'bold'
                  }}>Get Started &amp; Purchase &rsaquo;</small>
              </p>
            </Box>
          </Flex>
        </Container>
      </section>
    </Layout >
  )
}

export const query = graphql`
  query HomePage {
    allFile(filter: {relativeDirectory: {eq: "home"}}){
      nodes {
        id
        name
        publicURL
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          } 
        }
      }
    }
    home: wpPage(slug: {eq: "home"}) {
      title
      homepage {
        companies {
          logo {
            databaseId
            localFile {
              name
              childImageSharp {
                fixed(width: 190) {
                  ...GatsbyImageSharpFixed_noBase64
                }
              }
            }
          }
        }
        testimonial {
          __typename
          ... on WpTestimonial {
            testimonialACF {
              name
              title
              quote
              profile {
                localFile {
                  childImageSharp {
                    fluid {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
        pricing {
          title
          lineitems{
            item
          }
        }
      }
    }
    tour: wpPage(slug: { eq: "product-tour" }){
      productTour {
        sections {
          slug
          title
          subtitle
          excerpt
          icon {
            localFile {
              publicURL
            }
          }
        }
      }
    }
    connections: allWpConnection(
      filter: {
        slug: {
          in: [
            "dynamics-365-business-central-integration",
            "salesforce-connection",
            "excel-integration",
            "zendesk",
            "hubspot",
            "netsuite",
            "shopify",
            "stripe",
            "magento",
            "dynamics-365-sales-integration",
            "woocommerce"
          ]
        }
      }
    ) {
      nodes {
        databaseId
        title
        integrationSolutionDetails {
          wpcfIcon {
            localFile {
              publicURL
            }
          }
        }
      }
    }
    downloads: allWpDownload{
      nodes {
        databaseId
        title
        content
        featuredImage {
          node {
            localFile {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
        downloadFields {
          file {
            localFile {
              publicURL
            }
          }
        }
      }
    }
  }
`
export default connect()(IndexPage)