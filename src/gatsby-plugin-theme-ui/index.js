import { tailwind } from "@theme-ui/presets"

// const headers = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'].map(h => ({ [h]: { color: 'primary', fontFamily: 'heading' }})).reduce((curr, acc) => ({ ...acc, ...curr }), {})

export default {
  ...tailwind,
  borderStyles: {
    solid: 'solid',
    dashed: 'dashed',
    dotted: 'dotted' 
  },
  colors: {
    ...tailwind.colors,
    primary: '#304659',
    secondary: '#376AAD',
    lightblue: '#4591F8',
    lightgreen: '#82ac76'
  },
  fonts: {
    ...tailwind.fonts,
    body: `"Open Sans", ${tailwind.fonts.body}`,
    heading: `"Lato", ${tailwind.fonts.serif}`,
  },
  fontSizes: [
    '0.75rem',
    ...tailwind.fontSizes  
  ],
  layout: {
    container: {
      mx: 'auto',
      width: 'full',
      maxWidth: ['640px', '768px', '1024px', '1280px']
    }
  },
  buttons: {
    ...tailwind.buttons,
    primary: {
      fontFamily: 'heading',
      fontWeight: 'bold',
      px: 3,
      py: 2,
      bg: 'primary',
      color: 'white',
      borderRadius: 'full' 
    },
    secondary: {
      fontFamily: 'heading',
      fontWeight: 'bold',
      px: 3,
      py: 2,
      bg: 'secondary',
      color: 'white',
      borderRadius: 'full' 
    },
    inverse: {
      fontFamily: 'heading',
      fontWeight: 'bold',
      borderStyle: 'solid',
      borderWidth: 2,
      borderColor: 'lightblue',
      borderRadius: 'full',
      px: 3,
      py: 2,
      bg: 'transparent',
      color: 'lightblue'
    },
    more: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      fontFamily: 'heading',
      fontWeight: 'bold',
      borderStyle: 'solid',
      borderWidth: 1,
      borderColor: 'gray.4',
      borderRadius: 'full',
      px: 3,
      py: 2,
      bg: 'transparent',
      color: 'gray.6',
    },
    rounded: {
      fontFamily: 'heading',
      fontWeight: 'bold',
      borderStyle: 'solid',
      borderWidth: 1,
      borderRadius: 'full',
      bg: 'transparent',
      px: 3,
      py: 2,
    },
    blue: {
      fontFamily: 'heading',
      fontWeight: 'bold',
      fontSize: 0,
      borderStyle: 'solid',
      borderWidth: 1,
      borderRadius: 'full',
      borderColor: 'lightblue',
      color: 'white',
      bg: 'lightblue',
      px: 3,
      py: 2,
    },
    gray: {
      fontFamily: 'heading',
      fontWeight: 'bold',
      borderStyle: 'solid',
      borderWidth: 1,
      borderRadius: 'full',
      borderColor: 'gray.3',
      color: 'gray.8',
      bg: 'gray.3',
      px: 3,
      py: 2,
    },
    green: {
      fontFamily: 'heading',
      fontWeight: 'bold',
      fontSize: 0,
      borderStyle: 'solid',
      borderWidth: 1,
      borderRadius: 'full',
      borderColor: 'lightgreen',
      color: 'white',
      bg: 'lightgreen',
      px: 3,
      py: 2,
    }
  },
  forms: {
    ...tailwind.forms,
    select: {
      py: 0,
      lineHeight: 'tight',
      border: 0,
      borderRadius: 0,
      borderBottom: theme => `2px solid ${theme.colors.primary}`,
      '&:focus': {
        outline: 'none',
      },
    },
    checkbox: {
      color: 'gray.3',
      '&:focus': {
        color: 'lightblue'
      }
    }
  },
  styles: {
    ...tailwind.styles,
    root: {
      ...tailwind.styles.root,
      fontSize: 2,
      fontWeight: 'light',
      a: {
        color: 'secondary',
        textDecoration: 'none',
        ':hover': {
          textDecoration: 'underline'
        }
      },
      strong: {
        fontWeight: 'bold'
      },
      button: {
        borderRadius: 'full',
        '&:disabled': {
          cursor: 'not-allowed',
          opacity: 0.4
        },
        '&:focus': {
          outline: 0,
        },
        '&:hover': {
          cursor: 'pointer',
        }
      }
    }
  },
}